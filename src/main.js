/*
* Copyright (C) 2011 Sunil Mohan Adapa <sunil@medhas.org>.
* Copyright (C) 2011 O S K Chaitanya <osk@medhas.org>.
*
* Author: Sunil Mohan Adapa <sunil@medhas.org>
*         O S K Chaitanya <osk@medhas.org>
*
* This file is part of GNOME Nonogram.
*
* GNOME Nonogram is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* GNOME Nonogram is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with GNOME Nonogram. If not, see <http://www.gnu.org/licenses/>.
*/

imports.gi.versions.Gtk = "3.0";

const Gtk = imports.gi.Gtk;

const Config = imports.config;

// Adding path to enable loading nonogram io seed module
imports.searchPath.unshift(Config.libDir);

const MainWindow = imports.mainwindow;
const GameWidget = imports.gamewidget;
const DashboardWidget = imports.dashboardwidget;
const ClueTable = imports.cluetable;
const PixelGrid = imports.pixelgrid;
const Palette = imports.palette;

var builder;
var window;

function main() {
    Gtk.init(Seed.argv);

    builder = new Gtk.Builder();
    builder.add_from_file(GLib.build_filenamev([Config.uiDataDir,
                                                "ui-manager.ui"]));
    builder.add_from_file(GLib.build_filenamev([Config.uiDataDir,
                                                "main-window.ui"]));

    window = builder.get_object("mainwindow");
    window.connectSignalsAndChildren(builder);
    window.show_all();

    Gtk.main();
}

main();
