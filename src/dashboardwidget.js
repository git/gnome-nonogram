/*
* Copyright (C) 2011 Sunil Mohan Adapa <sunil@medhas.org>.
* Copyright (C) 2011 O S K Chaitanya <osk@medhas.org>.
*
* Author: Sunil Mohan Adapa <sunil@medhas.org>
*         O S K Chaitanya <osk@medhas.org>
*
* This file is part of GNOME Nonogram.
*
* GNOME Nonogram is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* GNOME Nonogram is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with GNOME Nonogram. If not, see <http://www.gnu.org/licenses/>.
*/

const Lang = imports.lang;
const GLib = imports.gi.GLib;
const GObject = imports.gi.GObject;
const Gio = imports.gi.Gio;
const Gdk = imports.gi.Gdk;
const GdkPixbuf = imports.gi.GdkPixbuf;
const Gtk = imports.gi.Gtk;

const Config = imports.config;
const Utils = imports.utils;

var _DashboardWidget;
var DashboardWidget = new GType(_DashboardWidget = {
    parent: Gtk.HPaned.type,
    name: "DashboardWidget",

    signals: [{name: "puzzle_activated",
               parameters: [GObject.TYPE_STRING]}],

    _GTK_TREE_SORTABLE_UNSORTED_SORT_COLUMN_ID: -2,

    _UNKNOWN_PIXEL_COLOR: 0x7f7f7fff,

    _PUZZLE_SETS_STORE_DISPLAY_NAME_COLUMN: 0,
    _PUZZLE_SETS_STORE_MARKUP_COLUMN: 1,
    _PUZZLE_SETS_STORE_ABSOLUTE_PATH_COLUMN: 2,
    _PUZZLE_SETS_STORE_INDEX_COLUMN: 3,
    _PUZZLE_SETS_STORE_STOCK_ID_COLUMN: 4,
    _PUZZLE_SETS_STORE_STOCK_ICON_SIZE_COLUMN: 5,
    _PUZZLES_STORE_DISPLAY_NAME_COLUMN: 0,
    _PUZZLES_STORE_ABSOLUTE_PATH_COLUMN: 1,
    _PUZZLES_STORE_PIXBUF_COLUMN: 2,
    _PUZZLES_STORE_INDEX_COLUMN: 3,

    _puzzleSetsListStore: null,
    _puzzleSetsTreeView: null,
    _puzzleSetsSelection: null,
    _puzzlesListStore: null,
    _puzzlesIconView: null,

    _puzzleSetCountScanIter: null,
    _settings: null,

    class_init: function(klass, prototype) {
        Utils.classInitBoilerplate(klass, prototype, _DashboardWidget);
    },

    init: function() {
        this._settings = new Gio.Settings({schema: "org.gnome.gnome-nonogram"});
        this.signal.realize.connect(Lang.bind(this, this._onRealize));
    },

    connectSignalsAndChildren: function(builder) {
        this._acquireChildren(builder);

        this._puzzleSetsSelection.set_mode(Gtk.SelectionMode.BROWSE);
        this._puzzleSetsSelection.signal.changed.connect(
            Lang.bind(this, this._onPuzzleSetSelectionChanged));

        this._puzzlesIconView.signal.selection_changed.connect(
            Lang.bind(this, this._onPuzzlesSelectionChanged));
        this._puzzlesIconView.signal.item_activated.connect(
            Lang.bind(this, this._onPuzzleActivated));
    },

    _acquireChildren: function(builder) {
        this._puzzleSetsListStore = builder.get_object("puzzle_sets_liststore");
        this._puzzleSetsTreeView = builder.get_object("puzzle_sets_treeview");
        this._puzzleSetsSelection = this._puzzleSetsTreeView.get_selection();

        this._puzzlesListStore = builder.get_object("puzzles_liststore");
        this._puzzlesIconView = builder.get_object("puzzles_iconview");
    },

    _onRealize: function() {
        this._scanForPuzzleSets(Config.puzzlesDataDir);
        this._selectPuzzleSet();
    },

    _isValidPuzzleSet: function(directory, fileInfo) {
        // FIXME: Since fileIfo.get_file_type() is returning junk, we are using
        // an alternate method.
        // We are now checking if the file has non-zero children upon
        // enumerating. If there are zero children, it is not a directory.
        // This also has a useful side-effect of not showing empty directories
        // in the dashboard
        var absolutePath = directory.resolve_relative_path(
            fileInfo.get_name()).get_path();

        var file = Gio.file_new_for_path(absolutePath);
        var enumerator = file.enumerate_children();

        var accept = true;
        if(enumerator.next_file() == null)
            accept = false;

        return accept;
    },

    _scanForPuzzleSets: function(directoryPath) {
        this._puzzleSetsListStore.clear();

        var files = this._getFilesInDirectory(directoryPath,
                                              this._isValidPuzzleSet);

        this._puzzleSetsListStore.set_sort_column_id(
            this._GTK_TREE_SORTABLE_UNSORTED_SORT_COLUMN_ID);

        for (var rowNumber = 0; rowNumber < files.length; ++rowNumber) {
            var displayName = this._normalizeName(files[rowNumber].displayName);

            var newRowIter = {};
            this._puzzleSetsListStore.append(newRowIter);
            newRowIter = newRowIter.value;
            this._puzzleSetsListStore.set_value(
                newRowIter,
                this._PUZZLE_SETS_STORE_DISPLAY_NAME_COLUMN,
                displayName);
            this._puzzleSetsListStore.set_value(
                newRowIter,
                this._PUZZLE_SETS_STORE_MARKUP_COLUMN,
                "<b>" + displayName + "</b>\n");
            this._puzzleSetsListStore.set_value(
                newRowIter,
                this._PUZZLE_SETS_STORE_ABSOLUTE_PATH_COLUMN,
                files[rowNumber].absolutePath);
            this._puzzleSetsListStore.set_value(
                newRowIter,
                this._PUZZLE_SETS_STORE_INDEX_COLUMN,
                [GObject.TYPE_UINT, files[rowNumber].index]);
            this._puzzleSetsListStore.set_value(
                newRowIter,
                this._PUZZLE_SETS_STORE_STOCK_ID_COLUMN,
                Gtk.STOCK_DIRECTORY);
            this._puzzleSetsListStore.set_value(
                newRowIter,
                this._PUZZLE_SETS_STORE_STOCK_ICON_SIZE_COLUMN,
                [GObject.TYPE_UINT, Gtk.IconSize.DIALOG]);
        }

        this._puzzleSetsListStore.set_sort_column_id(2, Gtk.SortType.ASCENDING);

        this._puzzleSetCountScanIter = null;
        var returnValue = {};
        var itemsAvailable =
            this._puzzleSetsListStore.get_iter_first(returnValue);
        if (itemsAvailable) {
            this._puzzleSetCountScanIter = returnValue.iter;
        }

        GLib.idle_add(GLib.PRIORITY_LOW,
                      Lang.bind(this, this._scanForPuzzleCountIdle));
    },

    _selectPuzzleSet: function() {
        var selectedPuzzleSetPath =
            this._settings.get_string("selected-puzzle-set");

        this._puzzleSetsListStore.foreach(Lang.bind(this,
            function(widget, treePath, iter) {
                var returnValue = {};
                this._puzzleSetsListStore.get_value(
                    iter,
                    this._PUZZLE_SETS_STORE_ABSOLUTE_PATH_COLUMN,
                    returnValue);

                var puzzleSetPath = returnValue.value.value;

                if (selectedPuzzleSetPath == "") {
                    selectedPuzzleSetPath = puzzleSetPath;
                }

                if (selectedPuzzleSetPath == puzzleSetPath) {
                    this._puzzleSetsSelection.select_iter(iter);
                    this._puzzleSetsTreeView.scroll_to_cell(treePath,
                                                            null,
                                                            true,
                                                            0.5,
                                                            0);
                }
            }
        ));
    },

    _scanForPuzzleCountIdle: function () {
        if (this._puzzleSetCountScanIter == null) {
            return false;
        }

        var puzzleSetPath = {};
        this._puzzleSetsListStore.get_value(
            this._puzzleSetCountScanIter,
            this._PUZZLE_SETS_STORE_ABSOLUTE_PATH_COLUMN,
            puzzleSetPath);
        puzzleSetPath = puzzleSetPath.value.value;
        var files = this._getFilesInDirectory(puzzleSetPath,
                                              this._isValidPuzzle);
        var displayName = {};
        this._puzzleSetsListStore.get_value(
            this._puzzleSetCountScanIter,
            this._PUZZLE_SETS_STORE_DISPLAY_NAME_COLUMN,
            displayName);
        displayName = displayName.value.value;
        var markup = "<b>" + displayName + "</b>\n<i><small>" +
            files.length + " puzzles</small></i>";
        this._puzzleSetsListStore.set_value(
            this._puzzleSetCountScanIter,
            this._PUZZLE_SETS_STORE_MARKUP_COLUMN,
            markup);

        var moreItemsAvailable = this._puzzleSetsListStore.iter_next(
            this._puzzleSetCountScanIter);
        if (moreItemsAvailable == false) {
            this._puzzleSetCountScanIter = null;
            return false;
        }

        return true;
    },

    _getFilesInDirectory: function (directoryPath, acceptChildCallback) {
        var directory = Gio.file_new_for_path(directoryPath);
        var attributes = Gio.FILE_ATTRIBUTE_STANDARD_DISPLAY_NAME + ","
                       + Gio.FILE_ATTRIBUTE_STANDARD_NAME + ","
                       + Gio.FILE_ATTRIBUTE_STANDARD_TYPE + ","
                       + Gio.FILE_ATTRIBUTE_STANDARD_CONTENT_TYPE;
        var enumerator = directory.enumerate_children(attributes, 0);

        var files = [];
        while (1) {
            var fileInfo = enumerator.next_file();
            if (fileInfo == null)
                break;

            if (typeof(acceptChildCallback) == "function") {
                if (acceptChildCallback(directory, fileInfo) == false)
                    continue;
            }

            var absolutePath = directory.resolve_relative_path(
                fileInfo.get_name()).get_path();

            var index =
                this._getIndexFromFileName(fileInfo.get_display_name());

            var file = {
                name: fileInfo.get_name(),
                displayName: fileInfo.get_display_name(),
                absolutePath: absolutePath,
                index: index
            };
            files.push(file);
        }

        return files;
    },

    _onPuzzleSetSelectionChanged: function(puzzleSetSelection) {
        puzzleSetSelection.selected_foreach(Lang.bind(this,
            function (model, path, iter) {
                var puzzleSetPath = {};
                this._puzzleSetsListStore.get_value(
                    iter,
                    this._PUZZLE_SETS_STORE_ABSOLUTE_PATH_COLUMN,
                    puzzleSetPath);
                puzzleSetPath = puzzleSetPath.value.value;
                this._scanForPuzzles(puzzleSetPath);
                this._settings.set_string("selected-puzzle-set", puzzleSetPath);
            })
        );

        this._selectPuzzle();
    },

    _isValidPuzzle: function(directoryPath, fileInfo) {
        var accept = Gio.content_type_is_a(fileInfo.get_content_type(),
                                           "image/*");
        return accept;
    },

    _scanForPuzzles: function(puzzleSetPath) {
        this._puzzlesListStore.clear();

        var files = this._getFilesInDirectory(puzzleSetPath,
                                              this._isValidPuzzle);

        this._puzzlesListStore.set_sort_column_id(
            this._GTK_TREE_SORTABLE_UNSORTED_SORT_COLUMN_ID);

        for (var rowNumber = 0; rowNumber < files.length; ++rowNumber) {
            var displayName = this._normalizeName(files[rowNumber].displayName);
            var pixbuf = this._getIconForPuzzle(files[rowNumber].absolutePath);

            var newRowIter = {};
            this._puzzlesListStore.append(newRowIter);
            newRowIter = newRowIter.value;
            this._puzzlesListStore.set_value(
                newRowIter,
                this._PUZZLES_STORE_DISPLAY_NAME_COLUMN,
                displayName);
            this._puzzlesListStore.set_value(
                newRowIter,
                this._PUZZLES_STORE_ABSOLUTE_PATH_COLUMN,
                files[rowNumber].absolutePath);
            this._puzzlesListStore.set_value(
                newRowIter,
                this._PUZZLES_STORE_PIXBUF_COLUMN,
                [GdkPixbuf.Pixbuf.type, pixbuf]);
            this._puzzlesListStore.set_value(
                newRowIter,
                this._PUZZLES_STORE_INDEX_COLUMN,
                [GObject.TYPE_UINT, files[rowNumber].index]);
        }

        this._puzzlesListStore.set_sort_column_id(3, Gtk.SortType.ASCENDING);
    },

    _normalizeName: function(puzzleName) {
        return puzzleName.replace(new RegExp("\\.[^.]*$"), "")
                         .replace(new RegExp("^[0-9]*[_-]+"), "")
                         .replace(new RegExp("[-_]+", "g"), " ")
                         .replace(new RegExp("(\\b)(\\w)", "g"),
                            function(fullMatch, boundaryMatch, letterMatch) {
                                return boundaryMatch + letterMatch.toUpperCase();
                            }
                          );
    },

    _getIconForPuzzle: function(puzzlePath) {
        var saveFilePath = Utils.getSaveFilePathForPuzzle(puzzlePath);

        try {
            var scaledPixbuf = this._makeIconForPuzzle(saveFilePath);
            return scaledPixbuf;
        } catch (e) {
            // Usually this means the puzzle hasn't been saved earlier.
            // It need not mean an error has occured.
            var pixbuf = new GdkPixbuf.Pixbuf.from_file(
                GLib.build_filenamev([Config.iconsDataDir,
                                      "unsolved-puzzle.png"]));
            return pixbuf;
        }
    },

    _makeIconForPuzzle: function(puzzleFile) {
        var pixbuf = new GdkPixbuf.Pixbuf.from_file(puzzleFile);
        var height = pixbuf.get_height();
        var width = pixbuf.get_width();

        var iconSize = Config.dashboardPuzzleIconSize;
        var scale = iconSize / Math.max(height, width);

        var destinationWidth = scale * width;
        var destinationHeight = scale * height;
        var destinationOffsetX = Math.floor((iconSize - destinationWidth) / 2);
        var destinationOffsetY = Math.floor((iconSize - destinationHeight) / 2);

        var scaledPixbuf = new GdkPixbuf.Pixbuf.c_new(GdkPixbuf.Colorspace.RGB,
                                                      true,
                                                      8,
                                                      iconSize,
                                                      iconSize);
        scaledPixbuf.fill(0x00000000);
        var backgroundPixbuf = new GdkPixbuf.Pixbuf.c_new(
            GdkPixbuf.Colorspace.RGB, true, 8, iconSize, iconSize);
        backgroundPixbuf.fill(this._UNKNOWN_PIXEL_COLOR);
        backgroundPixbuf.scale(scaledPixbuf,
                               destinationOffsetX,
                               destinationOffsetY,
                               Math.floor(destinationWidth),
                               Math.floor(destinationHeight),
                               0,
                               0,
                               1,
                               1,
                               GdkPixbuf.InterpType.NEAREST);

        pixbuf.composite(scaledPixbuf,
                         destinationOffsetX,
                         destinationOffsetY,
                         Math.floor(destinationWidth),
                         Math.floor(destinationHeight),
                         destinationOffsetX,
                         destinationOffsetY,
                         scale,
                         scale,
                         GdkPixbuf.InterpType.NEAREST,
                         255);

        return scaledPixbuf;
    },

    _getIndexFromFileName: function(fileName) {
        var match = fileName.match(new RegExp("^(\\d+)"));
        if (match == null)
            return 0;

        return parseInt(match[0], 10);
    },

    _onPuzzleActivated: function(iconview, path) {
        var iter = {};
        this._puzzlesListStore.get_iter(iter, path);
        iter = iter.value;
        var puzzlePath = {};
        this._puzzlesListStore.get_value(
            iter,
            this._PUZZLES_STORE_ABSOLUTE_PATH_COLUMN,
            puzzlePath);
        puzzlePath = puzzlePath.value.value;

        this.signal.puzzle_activated.emit(puzzlePath);
    },

    _onPuzzlesSelectionChanged: function(treeSelection) {
        if (this._puzzlesIconView.get_selected_items().length == 0) {
            this._settings.set_string("selected-puzzle", "");
            return;
        }

        this._puzzlesIconView.selected_foreach(Lang.bind(this,
            function(iconView, treePath) {
                var iter = {};
                this._puzzlesListStore.get_iter(iter, treePath);
                iter = iter.iter;

                var returnValue = {};
                this._puzzlesListStore.get_value(iter,
                    this._PUZZLES_STORE_ABSOLUTE_PATH_COLUMN,
                    returnValue);
                var puzzlePath = returnValue.value.value;

                this._settings.set_string("selected-puzzle", puzzlePath);
            }
        ));
    },

    _selectPuzzle: function() {
        var selectedPuzzlePath = this._settings.get_string("selected-puzzle");

        this._puzzlesListStore.foreach(Lang.bind(this,
            function(widget, treePath, iter) {
                var returnValue = {};
                this._puzzlesListStore.get_value(
                    iter,
                    this._PUZZLES_STORE_ABSOLUTE_PATH_COLUMN,
                    returnValue);

                var puzzlePath = returnValue.value.value;

                if (selectedPuzzlePath == "") {
                    selectedPuzzlePath = puzzlePath;
                }

                if (selectedPuzzlePath == puzzlePath) {
                    this._puzzlesIconView.select_path(treePath);
                    this._puzzlesIconView.scroll_to_path(treePath,
                                                         true,
                                                         0.5,
                                                         0.5);
                }
            }
        ));
    },

    getSelectedPuzzle: function() {
        return this._settings.get_string("selected-puzzle");
    },

    selectAndGetNextPuzzle: function() {
        var selectedItems = this._puzzlesIconView.get_selected_items();
        if (selectedItems.length == 0)
            return null;

        var selectedItemPath = selectedItems[0];
        var selectedItemIter = {};
        this._puzzlesListStore.get_iter(selectedItemIter, selectedItemPath);
        selectedItemIter = selectedItemIter.iter;

        var isNextItemAvailable = this._puzzlesListStore.iter_next(selectedItemIter);
        if (isNextItemAvailable == false) {
            return null;
        }

        selectedItemPath = this._puzzlesListStore.get_path(selectedItemIter);

        this._puzzlesIconView.select_path(selectedItemPath);
        return this.getSelectedPuzzle();
    },

    updateForPuzzle: function(pathOfUpdatedPuzzle) {
        if (pathOfUpdatedPuzzle == null)
            return;

        this._puzzlesListStore.foreach(Lang.bind(this,
            function(widget, treePath, iter) {
                var returnValue = {};
                this._puzzlesListStore.get_value(
                    iter,
                    this._PUZZLES_STORE_ABSOLUTE_PATH_COLUMN,
                    returnValue);

                var puzzlePath = returnValue.value.value;

                if (puzzlePath == pathOfUpdatedPuzzle) {
                    var pixbuf = this._getIconForPuzzle(puzzlePath);
                    this._puzzlesListStore.set_value(
                        iter,
                        this._PUZZLES_STORE_PIXBUF_COLUMN,
                        [GdkPixbuf.Pixbuf.type, pixbuf]);
                }
            }
        ));
    }
});
