/*
* Copyright (C) 2011 Sunil Mohan Adapa <sunil@medhas.org>.
* Copyright (C) 2011 O S K Chaitanya <osk@medhas.org>.
*
* Author: Sunil Mohan Adapa <sunil@medhas.org>
*         O S K Chaitanya <osk@medhas.org>
*
* This file is part of GNOME Nonogram.
*
* GNOME Nonogram is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* GNOME Nonogram is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with GNOME Nonogram. If not, see <http://www.gnu.org/licenses/>.
*/

const Lang = imports.lang;
const GLib = imports.gi.GLib;
const Gtk = imports.gi.Gtk;

const Config = imports.config;
const Utils = imports.utils;

var _MainWindow;
var MainWindow = new GType(_MainWindow = {
    parent: Gtk.Window.type,
    name: "MainWindow",

    _DASHBOARD_VIEW_PAGE: 0,
    _GAME_VIEW_PAGE: 1,

    _aboutDialog: null,
    _dashboardWidget: null,
    _gameWidget: null,
    _infoBar: null,
    _infoBarLabel: null,
    _infoBarButtonBox: null,
    _infoBarCloseButton: null,
    _infoBarSwitchToDashboardButton: null,
    _viewNotebook: null,
    _gameViewActionGroup: null,
    _undoAction: null,
    _redoAction: null,

    class_init: function(klass, prototype) {
        Utils.classInitBoilerplate(klass, prototype, _MainWindow);
    },

    init: function() {
    },

    connectSignalsAndChildren: function(builder) {
        var signalHandlers = {
            windowDelete: Lang.bind(this, this._windowDelete),
            tutorial: Lang.bind(this, this._tutorial),
            quit: Lang.bind(this, this._quit),
            helpContents: Lang.bind(this, this._helpContents),
            howToPlay: Lang.bind(this, this._howToPlay),
            about: Lang.bind(this, this._about),
            newGame: Lang.bind(this, this._newGame),
            newGameFromImage: Lang.bind(this, this._newGameFromImage),
            restartGame: Lang.bind(this, this._restartGame),
            switchToDashboardView: Lang.bind(this,
                this._switchToDashboardView),
            undo: Lang.bind(this, this._undo),
            redo: Lang.bind(this, this._redo)
        };

        builder.connect_signals(signalHandlers);

        this._acquireChildren(builder);

        this._dashboardWidget.connectSignalsAndChildren(builder);
        this._gameWidget.connectSignalsAndChildren(builder);

        this._viewNotebook.signal.switch_page.connect(
            Lang.bind(this, this._onViewNotebookSwitchPage));
        this._dashboardWidget.signal.puzzle_activated.connect(
            Lang.bind(this, this._onPuzzleActivated));
        this._gameWidget.signal.puzzle_full_status_changed.connect(
            Lang.bind(this, this._onPuzzleFullStatusChanged));
        this._gameWidget.signal.puzzle_solved.connect(
            Lang.bind(this, this._onPuzzleSolved));
        this._gameWidget.signal.puzzle_incorrect_solution.connect(
            Lang.bind(this, this._onPuzzleIncorrectSolution));
        this._gameWidget.signal.undo_redo_available.connect(
            Lang.bind(this, this._onUndoRedoAvailable));

        this._infoBar.signal.response.connect(
            Lang.bind(this, this._onInfoBarResponse));
    },

    _acquireChildren: function(builder) {
        var uiManager = builder.get_object("uimanager");
        var toolbar = uiManager.get_widget("/toolbar");
        toolbar.get_style_context().add_class(Gtk.STYLE_CLASS_PRIMARY_TOOLBAR);

        this._infoBar = builder.get_object("infobar");
        this._infoBarLabel = builder.get_object("infobar_label");
        this._infoBarButtonBox = builder.get_object("infobar_buttonbox");
        this._infoBarCloseButton = builder.get_object("infobar_close_button");
        this._infoBarSwitchToDashboardButton =
            builder.get_object("infobar_switch_to_dashboard_button");
        this._viewNotebook = builder.get_object("view_notebook");

        builder.add_from_file(GLib.build_filenamev([Config.uiDataDir,
                                                    "dashboard-view.ui"]));
        this._dashboardWidget = builder.get_object("dashboardwidget");
        this._viewNotebook.append_page(this._dashboardWidget);

        builder.add_from_file(GLib.build_filenamev([Config.uiDataDir,
                                                    "game-view.ui"]));
        var gameViewVBox = builder.get_object("game_view_vbox");
        this._viewNotebook.append_page(gameViewVBox);

        this._gameWidget = builder.get_object("gamewidget");

        builder.add_from_file(GLib.build_filenamev([Config.uiDataDir,
                                                    "about-dialog.ui"]));
        this._aboutDialog = builder.get_object("aboutdialog");
        this._aboutDialog.version = Config.version;
        this._aboutDialog.transient_for = this;

        // Some defaults that could not be set in the ui file
        this._gameViewActionGroup = builder.get_object("game_view_actiongroup");
        var actionList = this._gameViewActionGroup.list_actions();
        actionList.forEach(
            function(action) {
                var actionProxies = action.get_proxies();
                actionProxies.forEach(
                    function(proxyWidget) {
                        proxyWidget.no_show_all = true;
                    }
                );
            }
        );

        this._undoAction = builder.get_object("undo_action");
        this._redoAction = builder.get_object("redo_action");

        // Set white background for entire game area.
        // Viewport is the nearest ancestor for game widget which does
        // not have GTK_NO_WINDOW flag set.
        var viewPort = builder.get_object("game_viewport");
        viewPort.modify_bg(Gtk.StateType.NORMAL,
                           { red: 0xffff, green: 0xffff, blue: 0xffff });

        this._switchToDashboardView();
    },

    _windowDelete: function() {
        this._quit();
        return false;
    },

    _tutorial: function() {
        this._openHelp("tutorial");
    },

    _quit: function() {
        this._gameWidget.saveGame();

        Gtk.main_quit();
    },

    _helpContents: function() {
        this._openHelp();
    },

    _howToPlay: function() {
        this._openHelp();
    },

    _openHelp: function(section) {
        var url = "ghelp:gnome-nonogram";

        if (section != null)
            url += "?" + section;

        try {
            Gtk.show_uri(this.get_screen(), url, Gtk.get_current_event_time());
        } catch (e) {
            print("Unable to open help: " + url);
            print(e.message);
        }
    },

    _about: function() {
        this._aboutDialog.run();
        this._aboutDialog.hide();
    },

    _newGame: function() {
        var puzzlePath;
        if (this._viewNotebook.get_current_page() == this._GAME_VIEW_PAGE) {
            this._dashboardWidget.updateForPuzzle(this._gameWidget.puzzleFile);
            puzzlePath = this._dashboardWidget.selectAndGetNextPuzzle();
        } else {
            puzzlePath = this._dashboardWidget.getSelectedPuzzle();
        }

        if (puzzlePath != null) {
            this._startGame(puzzlePath);
        }
    },

    _newGameFromImage: function() {
        var fileChooserDialog = new Gtk.FileChooserDialog();
        fileChooserDialog.set_title("Open image file for puzzle");
        fileChooserDialog.add_button("Cancel", Gtk.ResponseType.CANCEL);
        fileChooserDialog.add_button("Open", Gtk.ResponseType.ACCEPT);
        fileChooserDialog.set_action(Gtk.FileChooserAction.OPEN);
        fileChooserDialog.set_transient_for(this);

        var response = fileChooserDialog.run();

        if (response == Gtk.ResponseType.ACCEPT) {
            var filename = fileChooserDialog.get_filename();
            this._startGame(filename);
        }

        fileChooserDialog.destroy();
    },

    _startGame: function(puzzlePath) {
        this._infoBar.hide();
        this._switchToGameView();
        this._gameWidget.puzzleFile = puzzlePath;
    },

    _restartGame: function() {
        this._infoBar.hide();
        this._gameWidget.restart();
    },

    _switchToGameView: function() {
        this._viewNotebook.set_current_page(this._GAME_VIEW_PAGE);
        this._gameViewActionGroup.set_visible(true);
    },

    _switchToDashboardView: function() {
        this._viewNotebook.set_current_page(this._DASHBOARD_VIEW_PAGE);
        this._gameViewActionGroup.set_visible(false);
        this._dashboardWidget.updateForPuzzle(this._gameWidget.puzzleFile);
    },

    _onViewNotebookSwitchPage: function() {
        this._infoBar.hide();
    },

    _onPuzzleActivated: function(object, puzzleFile) {
        this._startGame(puzzleFile);
    },

    _undo: function() {
        this._gameWidget.undo();
    },

    _redo: function() {
        this._gameWidget.redo();
    },

    _onPuzzleFullStatusChanged: function(gameWidget, puzzleFullStatus) {
        if (puzzleFullStatus == false) {
            this._infoBar.hide();
        }
    },

    _onPuzzleSolved: function() {
        this._showInfoBar(Gtk.MessageType.INFO,
                          "Congratulations! You have solved the puzzle!",
                          [this._infoBarSwitchToDashboardButton]);
    },

    _onPuzzleIncorrectSolution: function() {
        this._showInfoBar(Gtk.MessageType.WARNING,
            "Oops! Some of the clues don't match your solution. Please revisit your solution!");
    },

    _onUndoRedoAvailable: function(gameWidget, undoAvailable, redoAvailable) {
        this._undoAction.set_sensitive(undoAvailable);
        this._redoAction.set_sensitive(redoAvailable);
    },

    _showInfoBar: function(type, message, buttons) {
        this._infoBarButtonBox.foreach(
            function(widget) {
                widget.no_show_all = true;
                widget.hide();
            }
        );

        if (buttons != null) {
            buttons.forEach(
                function(button) {
                    button.no_show_all = false;
                }
            );
        }
        this._infoBarCloseButton.no_show_all = false;

        this._infoBar.message_type = type;
        this._infoBarLabel.label = message;
        this._infoBar.no_show_all = false;
        this._infoBar.show_all();
    },

    _onInfoBarResponse: function(infoBar, responseId) {
        if (responseId == Gtk.ResponseType.CLOSE) {
            this._infoBar.hide();
        }
    }
});
