/*
* Copyright (C) 2011 Sunil Mohan Adapa <sunil@medhas.org>.
* Copyright (C) 2011 O S K Chaitanya <osk@medhas.org>.
*
* Author: Sunil Mohan Adapa <sunil@medhas.org>
*         O S K Chaitanya <osk@medhas.org>
*
* This file is part of GNOME Nonogram.
*
* GNOME Nonogram is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* GNOME Nonogram is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with GNOME Nonogram. If not, see <http://www.gnu.org/licenses/>.
*/

const Lang = imports.lang;
const GLib = imports.gi.GLib;
const Gio = imports.gi.Gio;
const GObject = imports.gi.GObject;
const Gtk = imports.gi.Gtk;

const Config = imports.config;
const Utils = imports.utils;
const NonogramIO = imports.nonogramio;

var _GameWidget;
var GameWidget = new GType(_GameWidget = {
    parent: Gtk.Table.type,
    name: "GameWidget",
    properties: [
        {
            name: "puzzleFile",
            type: GObject.TYPE_STRING,
            default_value: null,
            flags: GObject.ParamFlags.READABLE
                 | GObject.ParamFlags.WRITABLE
                 | GObject.ParamFlags.CONSTRUCT
        }
    ],
    signals: [
        {
            name: "puzzle_solved"
        },
        {
            name: "puzzle_incorrect_solution"
        },
        {
            name: "puzzle_full_status_changed",
            parameters: [GObject.TYPE_BOOLEAN]
        },
        {
            name: "undo_redo_available",
            parameters: [GObject.TYPE_BOOLEAN, GObject.TYPE_BOOLEAN]
        }
    ],

    _BACKGROUND_COLOR: 0xffffffff,
    _NO_COLOR: 0,
    _MIN_PIXEL_SIZE: 10,

    __puzzleFile: null,
    _solutionPuzzlePixels: null,
    _numRows: 0,
    _numColumns: 0,

    _rowClueTable: null,
    _columnClueTable: null,
    _pixelGrid: null,
    _palette: null,

    _puzzleSaveTimeout: null,

    class_init: function(klass, prototype) {
        Utils.classInitBoilerplate(klass, prototype, _GameWidget);

        prototype.__defineGetter__("_puzzleFile", _GameWidget._getPuzzleFile);
        prototype.__defineSetter__("_puzzleFile", _GameWidget._setPuzzleFile);
        prototype.__defineGetter__("puzzleFile", _GameWidget._getPuzzleFile);
        prototype.__defineSetter__("puzzleFile", _GameWidget._setPuzzleFile);
    },

    init: function() {
    },

    connectSignalsAndChildren: function(builder) {
        this._acquireChildren(builder);

        this._pixelGrid.signal.puzzle_changed.connect(
            Lang.bind(this, this._onPuzzleChanged));
        this._pixelGrid.signal.puzzle_changed.connect(
            Lang.bind(this._rowClueTable, this._rowClueTable.onPuzzleChanged));
        this._pixelGrid.signal.puzzle_changed.connect(
            Lang.bind(this._columnClueTable,
                      this._columnClueTable.onPuzzleChanged));
        this._pixelGrid.signal.highlight_changed.connect(
            Lang.bind(this._rowClueTable,
                      this._rowClueTable.onHighlightChanged));
        this._pixelGrid.signal.highlight_changed.connect(
            Lang.bind(this._columnClueTable,
                      this._columnClueTable.onHighlightChanged));
        this._pixelGrid.signal.puzzle_full_status_changed.connect(
            Lang.bind(this, this._onPuzzleFullStatusChanged));
        this._pixelGrid.signal.undo_redo_available.connect(
            Lang.bind(this, this._emitUndoRedoAvailability));

        this._rowClueTable.signal.all_clues_satisfied.connect(
            Lang.bind(this, this._onClueTableAllCluesSatisfied));
        this._columnClueTable.signal.all_clues_satisfied.connect(
            Lang.bind(this, this._onClueTableAllCluesSatisfied));

        this._palette.connectSignalsAndChildren(builder);
        this._palette.signal.color_selected.connect(
            Lang.bind(this,
                function(palette, color) {
                    this._pixelGrid.drawingColor = color;
                }
            )
        );
    },

    restart: function() {
        this._pixelGrid.reset();
        this._pixelGrid.set_sensitive(true);
        this._rowClueTable.reset();
        this._columnClueTable.reset();

        this._markPuzzleUnsolved();
    },

    _acquireChildren: function(builder) {
        this._rowClueTable = builder.get_object("rowcluetable");
        this._columnClueTable = builder.get_object("columncluetable");
        this._pixelGrid = builder.get_object("pixelgrid");
        this._palette = builder.get_object("palette");
    },

    _getPuzzleFile: function() {
        return this.__puzzleFile;
    },

    _setPuzzleFile: function(value) {
        this.saveGame();

        try {
            this._solutionPuzzlePixels = NonogramIO.load_puzzle(value);
        } catch (e) {
            if (e.name == "PuzzleLoadFileError")
                print("File error while loading puzzle: " + e.message);
            else if (e.name == "PuzzleLoadImageError")
                print("Image error while loading puzzle: " + e.message);
            return;
        }

        this.__puzzleFile = value;
        this._numRows = this._solutionPuzzlePixels.length;
        this._numColumns = this._solutionPuzzlePixels[0].length;

        this._listColorIndices();

        var savedPuzzlePixels = this.loadGame(this.__puzzleFile);

        this._updateClueTables(savedPuzzlePixels);
        this._updatePixelGrid(savedPuzzlePixels);
        this._updatePalette();
    },

    _listColorIndices: function() {
        this._colorIndices = {};
        for (var y = 0; y < this._numRows; ++y) {
            for (var x = 0; x < this._numColumns; ++x) {
                var color = this._solutionPuzzlePixels[y][x];
                if (this._colorIndices[color] === undefined)
                    this._colorIndices[color] = 1;
                else
                    ++this._colorIndices[color];
            }
        }
    },

    _updateClueTables: function(savedPuzzlePixels) {
        this._rowClueTable.initializeClues(this._solutionPuzzlePixels,
                                           savedPuzzlePixels);
        this._columnClueTable.initializeClues(this._solutionPuzzlePixels,
                                              savedPuzzlePixels);
    },

    _updatePixelGrid: function(savedPuzzlePixels) {
        if (savedPuzzlePixels == null) {
            this._pixelGrid.createBlank(this._numColumns, this._numRows);
        } else {
            this._pixelGrid.createFromPixels(savedPuzzlePixels);
        }

        this._setPixelGridMinimumSize();

        if (savedPuzzlePixels != null &&
            this._isPuzzleMarkedAsSolved(this.__puzzleFile)) {
            this._pixelGrid.set_sensitive(false);
        } else {
            this._pixelGrid.set_sensitive(true);
        }
    },

    _updatePalette: function() {
        this._palette.populateFromColorIndices(this._colorIndices,
            this._BACKGROUND_COLOR);
    },

    _onPuzzleFullStatusChanged: function(pixelGrid, puzzleFullStatus) {
        this.signal.puzzle_full_status_changed.emit(puzzleFullStatus);

        if (puzzleFullStatus == true && this._isPuzzleSolved() == false)
            this._onPuzzleIncorrectSolution();
    },

    _isPuzzleSolved: function() {
        if (this._rowClueTable.allClueLinesAreSatisfied &&
            this._columnClueTable.allClueLinesAreSatisfied) {
            return true;
        }

        return false;
    },

    _onPuzzleChanged: function() {
        this._removePendingSaveTimeout();

        this._puzzleSaveTimeout = GLib.timeout_add(GLib.PRIORITY_LOW,
            Config.saveIntervalMilliseconds,
            Lang.bind(this, this._onPuzzleSaveScheduleTimeout));
    },

    _removePendingSaveTimeout: function() {
        if (this._puzzleSaveTimeout != null) {
            GLib.source_remove(this._puzzleSaveTimeout);
            this._puzzleSaveTimeout = null;
        }
    },

    _onPuzzleSolved: function() {
        this._pixelGrid.set_sensitive(false);
        this._pixelGrid.revealRemaining();
        this._rowClueTable.markAllCluesSatisfied();
        this._columnClueTable.markAllCluesSatisfied();

        this._markPuzzleSolved();
        this.signal.puzzle_solved.emit();
    },

    _onPuzzleIncorrectSolution: function() {
        this.signal.puzzle_incorrect_solution.emit();
    },

    _onClueTableAllCluesSatisfied: function() {
        if (this._isPuzzleSolved())
            this._onPuzzleSolved();
    },

    _setPixelGridMinimumSize: function() {
        if (this._numRows == 0 || this._numColumns == 0) {
            return;
        }

        var rowClueTableMinimumHeight =
            this._rowClueTable.get_preferred_height().minimum_height;
        var columnClueTableMinimumWidth =
            this._columnClueTable.get_preferred_width().minimum_width;

        var columnClueLineWidth = columnClueTableMinimumWidth
                                  / this._numColumns;
        var rowClueLineHeight = rowClueTableMinimumHeight
                                / this._numRows;

        var pixelSize = Math.max(
            columnClueLineWidth, rowClueLineHeight, this._MIN_PIXEL_SIZE);

        this._pixelGrid.set_size_request(pixelSize * this._numColumns,
                                         pixelSize * this._numRows);
    },

    undo: function() {
        this._pixelGrid.undo();
    },

    redo: function() {
        this._pixelGrid.redo();
    },

    _emitUndoRedoAvailability: function(pixelGrid,
                                        undoAvailable,
                                        redoAvailable) {
        this.signal.undo_redo_available.emit(undoAvailable, redoAvailable);
    },

    _onPuzzleSaveScheduleTimeout: function() {
        this.saveGame();
        return false;
    },

    saveGame: function() {
        if (this.__puzzleFile == null)
            return;

        this._removePendingSaveTimeout();

        try {
            this._createSaveDirForPuzzle(this._puzzleFile);

            var savePath = Utils.getSaveFilePathForPuzzle(this.__puzzleFile)
            NonogramIO.save_puzzle(savePath, this._pixelGrid.puzzlePixels);
        } catch (e) {
            // TODO: Indicate error
            print("Unable to save puzzle progress: " + e.message);
        }
    },

    loadGame: function() {
        var saveFilePath = Utils.getSaveFilePathForPuzzle(this.__puzzleFile);
        try {
            var savedPuzzlePixels = NonogramIO.load_puzzle(saveFilePath);
            return savedPuzzlePixels;
        } catch (e) {
            // Usually this means the puzzle hasn't been saved earlier.
            // It need not mean an error has occured.
            return null;
        }
    },

    _markPuzzleSolved: function() {
        try {
            this._createSaveDirForPuzzle(this._puzzleFile);

            var saveFilePath =
                Utils.getSaveFilePathForPuzzle(this.__puzzleFile);
            var markFilePath = saveFilePath + ".solved";
            var file = Gio.file_new_for_path(markFilePath);
            file.replace(null, false, 2, null);
        } catch (e) {
            // TODO: Indicate error
            print("Unable to mark puzzle as solved: " + e.message);
        }
    },

    _markPuzzleUnsolved: function() {
        try {
            var saveFilePath =
                Utils.getSaveFilePathForPuzzle(this.__puzzleFile);
            var markFilePath = saveFilePath + ".solved";
            var file = Gio.file_new_for_path(markFilePath);
            file.delete(null);
        } catch (e) {
            // Usually this means the puzzle hasn't been solved earlier.
            // It need not mean an error has occured.
        }
    },

    _isPuzzleMarkedAsSolved: function() {
        try {
            var saveFilePath =
                Utils.getSaveFilePathForPuzzle(this.__puzzleFile);
            var markFilePath = saveFilePath + ".solved";
            var file = Gio.file_new_for_path(markFilePath);
            return file.query_exists(null);
        } catch (e) {
            // Usually this means the puzzle hasn't been solved earlier.
            // It need not mean an error has occured.
            return false;
        }
    },

    _createSaveDirForPuzzle: function(puzzlePath) {
        var names = Utils.getPuzzleSetAndPuzzleNames(puzzlePath);
        var saveDirPath = GLib.build_filenamev([Config.saveDataDir,
                                                names.puzzleSetName]);
        var saveDir = Gio.file_new_for_path(saveDirPath);

        var alreadyExists = saveDir.query_exists(null);
        if (alreadyExists) {
            return;
        }

        saveDir.make_directory_with_parents(null);
    }
});
