/*
* Copyright (C) 2011 Sunil Mohan Adapa <sunil@medhas.org>.
* Copyright (C) 2011 O S K Chaitanya <osk@medhas.org>.
*
* Author: Sunil Mohan Adapa <sunil@medhas.org>
*         O S K Chaitanya <osk@medhas.org>
*
* This file is part of GNOME Nonogram.
*
* GNOME Nonogram is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* GNOME Nonogram is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with GNOME Nonogram. If not, see <http://www.gnu.org/licenses/>.
*/

#include <glib.h>
#include <gdk-pixbuf/gdk-pixbuf.h>
#include <gtk/gtk.h>
#include <seed.h>

#define SUPPORTED_BITS_PER_SAMPLE 8
#define SUPPORTED_COLORSPACE GDK_COLORSPACE_RGB

static GdkPixbuf * load_puzzle_from_file(guchar *path,
                                         SeedContext ctx,
                                         SeedException *exception);
static SeedValue puzzle_new_from_pixbuf(const GdkPixbuf *pixbuf,
                                        SeedContext ctx,
                                        SeedException *exception);
static gboolean save_puzzle_to_file(guchar * path,
                                    SeedValue puzzlePixels,
                                    SeedContext ctx,
                                    SeedException *exception);
SeedValue
load_puzzle(SeedContext ctx,
            SeedObject function,
            SeedObject this_object,
            gsize argument_count,
            const SeedValue arguments[],
            SeedException *exception)
{
  guchar *path;
  GdkPixbuf *pixbuf;
  SeedValue puzzle;

  // FIXME: Use CHECK_ARG_COUNT macro instead.
  // Currently, including seed-module.h results in build failure because
  // of mismatched include path and location of header file.
  if (argument_count != 1)
    {
      seed_make_exception(ctx, exception, "ArgumentError",
                          "Wrong number of arguments; expected %u, got %lu",
                          1, (unsigned long) argument_count);
      return seed_make_undefined(ctx);
    }

  path = seed_value_to_string(ctx, arguments[0], exception);
  if (path == NULL)
    return seed_make_undefined(ctx);

  pixbuf = load_puzzle_from_file(path, ctx, exception);
  if (pixbuf == NULL)
    return seed_make_undefined(ctx);

  puzzle = puzzle_new_from_pixbuf(pixbuf, ctx, exception);
  if (puzzle == NULL)
    return seed_make_undefined(ctx);

  g_free(path);

  return puzzle;
}

static GdkPixbuf *
load_puzzle_from_file(guchar * path,
                      SeedContext ctx,
                      SeedException *exception)
{
  GError *error = NULL;
  GdkPixbuf *pixbuf = gdk_pixbuf_new_from_file(path, &error);
  if (pixbuf == NULL)
    {
      char *error_type = "PuzzleLoadFileError";
      if (error->domain == GDK_PIXBUF_ERROR)
        error_type = "PuzzleLoadImageError";

      seed_make_exception(ctx, exception, error_type, "%s", error->message);
      g_error_free(error);
      return NULL;
    }

  return pixbuf;
}

static SeedValue
puzzle_new_from_pixbuf(const GdkPixbuf *pixbuf,
                       SeedContext ctx,
                       SeedException *exception)
{
  int bits_per_sample = gdk_pixbuf_get_bits_per_sample(pixbuf);
  if (bits_per_sample != SUPPORTED_BITS_PER_SAMPLE)
    {
      seed_make_exception(ctx, exception, "PuzzleLoadImageError",
                          "Image has %d bits per sample, %d expected",
                          bits_per_sample, SUPPORTED_BITS_PER_SAMPLE);
      return NULL;
    }

  int num_channels = gdk_pixbuf_get_n_channels(pixbuf);
  if (num_channels != 3 && num_channels != 4)
    {
      seed_make_exception(ctx, exception, "PuzzleLoadImageError",
                          "Image has %d channels, 3 or 4 channels expected",
                          num_channels);
      return NULL;
    }

  GdkColorspace colorspace = gdk_pixbuf_get_colorspace(pixbuf);
  if (colorspace != SUPPORTED_COLORSPACE)
    {
      seed_make_exception(ctx, exception, "PuzzleLoadImageError",
                          "Image colorspace not supported, expected RGB");
      return NULL;
    }

  guchar *pixels = gdk_pixbuf_get_pixels(pixbuf);
  guint width = gdk_pixbuf_get_width(pixbuf);
  guint height = gdk_pixbuf_get_height(pixbuf);
  guint rowstride = gdk_pixbuf_get_rowstride(pixbuf);

  SeedValue puzzle_seed_array;
  SeedValue puzzle[height];
  int x, y;
  for (y = 0; y < height; ++y)
    {
      SeedValue row[width];
      for (x = 0; x < width; ++x)
        {
          guint32 color;
          guint32 r = pixels[y * rowstride + x * num_channels + 0];
          guint32 g = pixels[y * rowstride + x * num_channels + 1];
          guint32 b = pixels[y * rowstride + x * num_channels + 2];
          guint32 a = 255;
          if (num_channels == 4)
            a = pixels[y * rowstride + x * num_channels + 3];

          color = (r << 24) + (g << 16) + (b << 8) + a;

          row[x] = seed_value_from_uint(ctx, color, exception);
          if (row[x] == NULL)
            return NULL;
        }
      puzzle[y] = seed_make_array(ctx, row, width, exception);
      if (puzzle[y] == NULL)
        return NULL;
    }

  puzzle_seed_array = seed_make_array(ctx, puzzle, height, exception);
  return puzzle_seed_array;
}

SeedValue
save_puzzle(SeedContext ctx,
            SeedObject function,
            SeedObject this_object,
            gsize argument_count,
            const SeedValue arguments[],
            SeedException *exception)
{
  // FIXME: Use CHECK_ARG_COUNT macro instead.
  // Currently, including seed-module.h results in build failure because
  // of mismatched include path and location of header file.
  if (argument_count != 2)
    {
      seed_make_exception(ctx,
                          exception,
                          "ArgumentError",
                          "Wrong number of arguments; expected %u, got %lu",
                          2,
                          (unsigned long) argument_count);
      return seed_make_undefined(ctx);
    }

  guchar *path = seed_value_to_string(ctx, arguments[0], exception);
  if (path == NULL)
    return seed_make_undefined(ctx);

  SeedValue puzzlePixels = arguments[1];

  save_puzzle_to_file(path, puzzlePixels, ctx, exception);

  g_free(path);

  return seed_make_undefined(ctx);
}

static gboolean
save_puzzle_to_file(guchar * path,
                    SeedValue puzzlePixels,
                    SeedContext ctx,
                    SeedException *exception)
{
  SeedValue js_height = seed_object_get_property(ctx, puzzlePixels, "length");
  int height = seed_value_to_uint(ctx, js_height, exception);

  SeedValue first_row = seed_object_get_property_at_index(ctx,
                                                          puzzlePixels,
                                                          0,
                                                          exception);
  SeedValue js_width = seed_object_get_property(ctx, first_row, "length");
  int width = seed_value_to_uint(ctx, js_width, exception);

  GdkPixbuf * pixbuf = gdk_pixbuf_new(GDK_COLORSPACE_RGB,
                                      TRUE,
                                      8,
                                      width,
                                      height);
  if (pixbuf == NULL)
    {
      seed_make_exception(ctx,
                          exception,
                          "PuzzleSaveError",
                          "Unable to create image buffer");
      return FALSE;
    }

  guchar * pixels = gdk_pixbuf_get_pixels(pixbuf);
  int rowstride = gdk_pixbuf_get_rowstride(pixbuf);
  int n_channels = 4;

  for (int y = 0; y < height; y++)
    {
      SeedValue row = seed_object_get_property_at_index(ctx,
                                                        puzzlePixels,
                                                        y,
                                                        exception);
      for (int x = 0; x < width; x++)
        {
          SeedValue js_color = seed_object_get_property_at_index(ctx,
                                                                 row,
                                                                 x,
                                                                 exception);
          guint color = seed_value_to_uint(ctx, js_color, exception);
          guchar red = (color >> 24) & 0xff;
          guchar green = (color >> 16) & 0xff;
          guchar blue = (color >> 8) & 0xff;
          guchar alpha = color & 0xff;

          guchar * current_pixel = pixels + (y * rowstride) + (x * n_channels);
          current_pixel[0] = red;
          current_pixel[1] = green;
          current_pixel[2] = blue;
          current_pixel[3] = alpha;
        }
    }

  GError *error = NULL;
  gboolean success = gdk_pixbuf_save(pixbuf, path, "png", &error, NULL);
  if (success == FALSE)
    {
      seed_make_exception(ctx,
                          exception,
                          "PuzzleSaveError",
                          "Unable to save image: %s",
                          error->message);
      g_error_free(error);
      return FALSE;
    }

  return TRUE;
}

seed_static_function static_functions[] = {
  {"load_puzzle", load_puzzle, 0},
  {"save_puzzle", save_puzzle, 0},
  {NULL, NULL, 0}
};

SeedObject
seed_module_init(SeedEngine *engine)
{
  SeedObject nonogram_io;
  SeedGlobalContext *ctx = engine->context;

  seed_class_definition class_def = seed_empty_class;
  class_def.static_functions = static_functions;

  SeedClass nonogram_io_class = seed_create_class(&class_def);

  nonogram_io = seed_make_object(ctx, nonogram_io_class, NULL);
  seed_value_protect(ctx, nonogram_io);

  return nonogram_io;
}
