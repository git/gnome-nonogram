/*
* Copyright (C) 2011 Sunil Mohan Adapa <sunil@medhas.org>.
* Copyright (C) 2011 O S K Chaitanya <osk@medhas.com>.
*
* Author: Sunil Mohan Adapa <sunil@medhas.org>
*         O S K Chaitanya <osk@medhas.org>
*
* This file is part of GNOME Nonogram.
*
* GNOME Nonogram is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* GNOME Nonogram is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with GNOME Nonogram. If not, see <http://www.gnu.org/licenses/>.
*/

const Lang = imports.lang;
const Cairo = imports.cairo;
const GObject = imports.gi.GObject;
const Gtk = imports.gi.Gtk;

const Utils = imports.utils;

var _Palette;
var Palette = new GType(_Palette = {
    parent:Gtk.HButtonBox.type,
    name: "Palette",

    signals: [
        {
            name: "color_selected",
            parameters: [GObject.TYPE_UINT]
        }
    ],

    _NO_COLOR: 0,

    _GDK_KEY_space: 0x020,
    _GDK_KEY_0: 0x030,
    _GDK_KEY_1: 0x031,
    _GDK_KEY_2: 0x032,
    _GDK_KEY_3: 0x033,
    _GDK_KEY_4: 0x034,
    _GDK_KEY_5: 0x035,
    _GDK_KEY_6: 0x036,
    _GDK_KEY_7: 0x037,
    _GDK_KEY_8: 0x038,
    _GDK_KEY_9: 0x039,
    _GDK_KEY_Q: 0x051,

    _accelGroup: null,

    class_init: function(klass, prototype) {
        Utils.classInitBoilerplate(klass, prototype, _Palette);
    },

    init: function() {
    },

    connectSignalsAndChildren: function(builder) {
        this._acquireChildren(builder);
    },

    _acquireChildren: function(builder) {
        this._accelGroup = builder.get_object("palette_accelgroup");
    },

    populateFromColorIndices: function(colorIndices, backgroundColor) {
        // Remove existing children
        this.foreach(
            function(child) {
                child.get_parent().remove(child);
            }
        );

        var keyIndex = 0;
        // add button for "unknown" pixel color
        var radioButtonGroupSource = this._addButton(this._NO_COLOR,
                                                     keyIndex,
                                                     " ",
                                                     "Eraser",
                                                     null);
        ++keyIndex;
        this._addButton(backgroundColor,
                        keyIndex,
                        " ",
                        "Background Pen",
                        radioButtonGroupSource);
        ++keyIndex;

        var separator = new Gtk.Separator(
            {orientation: Gtk.Orientation.VERTICAL});
        this.pack_start(separator, false, false);

        // sort so that buttons can be placed in decreasing order of frequency
        // of occurence of the color in the solution image
        var sortedColors = Object.keys(colorIndices).sort(
            function(a, b) {
                return colorIndices[b] - colorIndices[a];
            }
        );

        // add new ones with color (one for each color in the puzzle)
        var firstForegroundButton = null;
        sortedColors.forEach(Lang.bind(this,
            function(color) {
                if (color == backgroundColor)
                    return;

                var button = this._addButton(color,
                                             keyIndex,
                                             (keyIndex - 1).toString(),
                                             "Foreground Pen",
                                             radioButtonGroupSource);

                if (firstForegroundButton == null)
                    firstForegroundButton = button;

                ++keyIndex;
            }
        ));

        if (firstForegroundButton != null)
            firstForegroundButton.active = true;

        this.show_all();
    },

    _addButton: function(color,
                         keyIndex,
                         labelText,
                         tooltipText,
                         radioButtonGroupSource) {
        var button = new PaletteButton({
            draw_indicator: false,
            halign: Gtk.Align.CENTER
        });

        if (radioButtonGroupSource != null) {
            button.join_group(radioButtonGroupSource);
        }

        button.setColor(parseInt(color));
        button.set_label(labelText);

        button.signal.clicked.connect(Lang.bind(this, this._onButtonClicked));
        this.pack_start(button, false, false);

        if (keyIndex < 0 || keyIndex > 11)
            return button;

        var gdkKeyVal;
        switch (keyIndex) {
            case 0:
                gdkKeyVal = this._GDK_KEY_Q;
                break;

            case 1:
                gdkKeyVal = this._GDK_KEY_space;
                break;

            case 2:
                gdkKeyVal = this._GDK_KEY_1;
                break;

            case 3:
                gdkKeyVal = this._GDK_KEY_2;
                break;

            case 4:
                gdkKeyVal = this._GDK_KEY_3;
                break;

            case 5:
                gdkKeyVal = this._GDK_KEY_4;
                break;

            case 6:
                gdkKeyVal = this._GDK_KEY_5;
                break;

            case 7:
                gdkKeyVal = this._GDK_KEY_6;
                break;

            case 8:
                gdkKeyVal = this._GDK_KEY_7;
                break;

            case 9:
                gdkKeyVal = this._GDK_KEY_8;
                break;

            case 10:
                gdkKeyVal = this._GDK_KEY_9;
                break;

            case 11:
                gdkKeyVal = this._GDK_KEY_0;
                break;
        }

        button.add_accelerator("clicked", this._accelGroup, gdkKeyVal, 0, 0);

        if (tooltipText == null)
            tooltipText = "";
        else
            tooltipText += "\n";

        tooltipText += "<i>Shortcut key: "
                    + Gtk.accelerator_name(gdkKeyVal, 0)
                    + "</i>";
        button.set_tooltip_markup(tooltipText);

        return button;
    },

    _onButtonClicked: function(button) {
        this.signal.color_selected.emit(button.getColor());
    }
});

// TODO: See if we can make a property called "color" with a setter and getter.
//       Attempting to do this has caused mysterious crashes at the moment.
var _PaletteButton;
var PaletteButton = new GType(_PaletteButton = {
    parent: Gtk.RadioButton.type,
    name: "PaletteButton",

    _NO_COLOR: 0,
    _UNKNOWN_PIXEL_COLOR: 0x7f7f7fff,

    __color: null,
    _drawingArea: null,

    class_init: function(klass, prototype) {
        Utils.classInitBoilerplate(klass, prototype, _PaletteButton);
    },

    init: function() {
    },

    getColor: function() {
        return this.__color;
    },

    setColor: function(value) {
        this.__color = value;

        this._drawingArea = new Gtk.DrawingArea();
        this._drawingArea.set_size_request(32, 16);
        this._drawingArea.signal.draw.connect(
            Lang.bind(this, this._drawColor));

        this.set_image(this._drawingArea);
    },

    _drawColor: function(widget, cairoContextStruct) {
        var cairoContext = new Cairo.Context.steal(cairoContextStruct);
        this._cairoContext = cairoContext; // FIXME: this is only to keep a ref
                                           // until the next call

        var size = this._drawingArea.get_allocation().allocation;
        var color = this.__color;
        if (color == this._NO_COLOR)
            color = this._UNKNOWN_PIXEL_COLOR;

        var r = ((color >> 24) & 0xff) / 255.0;
        var g = ((color >> 16) & 0xff) / 255.0;
        var b = ((color >> 8) & 0xff) / 255.0;

        cairoContext.set_source_rgb(r, g, b);
        cairoContext.rectangle(0, 0, size.width, size.height);
        cairoContext.fill();

        cairoContext.set_source_rgb(0.0, 0.0, 0.0);
        cairoContext.rectangle(0, 0, size.width, size.height);
        cairoContext.stroke();

        return false;
    }
});
