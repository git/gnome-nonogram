/*
* Copyright (C) 2011 Sunil Mohan Adapa <sunil@medhas.org>.
* Copyright (C) 2011 O S K Chaitanya <osk@medhas.org>.
*
* Author: Sunil Mohan Adapa <sunil@medhas.org>
*         O S K Chaitanya <osk@medhas.org>
*
* This file is part of GNOME Nonogram.
*
* GNOME Nonogram is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* GNOME Nonogram is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with GNOME Nonogram. If not, see <http://www.gnu.org/licenses/>.
*/

const Lang = imports.lang;
const Cairo = imports.cairo;
const GObject = imports.gi.GObject;
const Gdk = imports.gi.Gdk;
const Gtk = imports.gi.Gtk;

const Utils = imports.utils;

var _PixelGrid;
var PixelGrid = new GType(_PixelGrid = {
    parent: Gtk.DrawingArea.type,
    name: "PixelGrid",
    properties: [
        {
            // FIXME: This may not be accessible in the C interface
            name: "puzzlePixels",
            type: GObject.TYPE_ARRAY,
            default_value: null,
            flags: GObject.ParamFlags.READABLE
                 | GObject.ParamFlags.WRITABLE
                 | GObject.ParamFlags.CONSTRUCT
        },
        {
            name: "drawingColor",
            type: GObject.TYPE_UINT,
            default_value: null,
            flags: GObject.ParamFlags.READABLE
                 | GObject.ParamFlags.WRITABLE
                 | GObject.ParamFlags.CONSTRUCT
        }
    ],

    signals: [
        // Maintain this order to work around argument count bug in seed
        {
            name: "puzzle_full_status_changed",
            parameters: [GObject.TYPE_BOOLEAN]
        },
        {
            name: "puzzle_changed",
            parameters: [GObject.TYPE_OBJECT]
        },
        {
            name: "highlight_changed",
            parameters: [GObject.TYPE_OBJECT]
        },
        {
            name: "undo_redo_available",
            parameters: [GObject.TYPE_BOOLEAN, GObject.TYPE_BOOLEAN]
        }
    ],

    _UNKNOWN_PIXEL_COLOR: 0x7f7f7fff,
    _BACKGROUND_COLOR: 0xffffffff,
    _NO_COLOR: 0,

    __puzzlePixels: null,
    __drawingColor: null,
    _unknownPixelsCount: 0,

    _pixelUnderPointer: null,
    _dragStartPosition: null,
    _dragEndPosition: null,
    _pixelChangesForUndo: null,

    _undoStack: [],
    _redoStack: [],

    class_init: function(klass, prototype) {
        Utils.classInitBoilerplate(klass, prototype, _PixelGrid);

        prototype.__defineGetter__("_puzzlePixels",
                                   _PixelGrid._getPuzzlePixels);
        prototype.__defineSetter__("_puzzlePixels",
                                   _PixelGrid._setPuzzlePixels);
        prototype.__defineGetter__("puzzlePixels", _PixelGrid._getPuzzlePixels);
        prototype.__defineSetter__("puzzlePixels", _PixelGrid._setPuzzlePixels);

        prototype.__defineGetter__("_drawingColor",
                                   _PixelGrid._getDrawingColor);
        prototype.__defineSetter__("_drawingColor",
                                   _PixelGrid._setDrawingColor);
        prototype.__defineGetter__("drawingColor", _PixelGrid._getDrawingColor);
        prototype.__defineSetter__("drawingColor", _PixelGrid._setDrawingColor);
    },

    init: function() {
        this.add_events(Gdk.EventMask.BUTTON_PRESS_MASK);
        this.add_events(Gdk.EventMask.BUTTON_RELEASE_MASK);
        this.add_events(Gdk.EventMask.POINTER_MOTION_MASK);
        this.add_events(Gdk.EventMask.POINTER_MOTION_HINT_MASK);

        this.signal.draw.connect(Lang.bind(this, this._onDraw));
        this.signal.button_press_event.connect(
            Lang.bind(this, this._onButtonPress));
        this.signal.button_release_event.connect(
            Lang.bind(this, this._onButtonRelease));
        this.signal.motion_notify_event.connect(
            Lang.bind(this, this._onMouseMove));
        this.signal.state_flags_changed.connect(
            Lang.bind(this, this._onStateFlagsChanged));
    },

    createBlank: function(columns, rows) {
        this._numRows = rows;
        this._numColumns = columns;
        this.reset();
    },

    createFromPixels: function(pixels) {
        this.puzzlePixels = pixels;
    },

    reset: function() {
        this.__puzzlePixels = [];
        for (var y = 0; y < this._numRows; ++y) {
            this.__puzzlePixels[y] = [];
            for (var x = 0; x < this._numColumns; ++x) {
                this.__puzzlePixels[y][x] = this._NO_COLOR;
            }
        }

        this.resetUndoRedo();
        this._unknownPixelsCount = this._numRows * this._numColumns;

        this.queue_draw();
    },

    resetUndoRedo: function() {
        this._undoStack = [];
        this._redoStack = [];
        this._emitUndoRedoAvailable(null, null);
    },

    _getPuzzlePixels: function() {
        return this.__puzzlePixels;
    },

    _setPuzzlePixels: function(puzzlePixels) {
        this.__puzzlePixels = puzzlePixels;

        this._numRows = this._puzzlePixels.length;
        this._numColumns = this._puzzlePixels[0].length;

        this.resetUndoRedo();
        this._countUnknownPixels();

        this.queue_draw();
    },

    _getDrawingColor: function() {
        return this.__drawingColor;
    },

    _setDrawingColor: function(value) {
        this.__drawingColor = value;
    },

    _countUnknownPixels: function() {
        this._unknownPixelsCount = 0;

        for (var y = 0; y < this._numRows; ++y) {
            for (var x = 0; x < this._numColumns; ++x) {
                if (this._puzzlePixels[y][x] == this._NO_COLOR)
                    ++this._unknownPixelsCount;
            }
        }
    },

    _onDraw: function(widget, cairoContextStruct) {
        var cairoContext = new Cairo.Context.steal(cairoContextStruct);
        this._cairoContext = cairoContext; // FIXME: this is only to keep a ref
                                           // until the next call

        var size = widget.get_allocation().allocation;
        cairoContext.scale(size.width / this._numColumns,
                           size.height / this._numRows);

        for (var i = 0; i < this._numRows; ++i) {
            for (var j = 0; j < this._numColumns; ++j) {
                var color = this._puzzlePixels[i][j];
                if (color == this._NO_COLOR)
                    color = this._UNKNOWN_PIXEL_COLOR;

                var r = ((color >> 24) & 0xff) / 255.0;
                var g = ((color >> 16) & 0xff) / 255.0;
                var b = ((color >> 8) & 0xff) / 255.0;

                cairoContext.set_source_rgb(r, g, b);
                cairoContext.rectangle(j, i, 1, 1);
                cairoContext.fill();
            }
        }

        if (this._pixelUnderPointer != null) {
            var highlightEdgeThickness = 10.0 * this._numColumns / size.width;

            var pattern = new Cairo.LinearGradient(
                this._pixelUnderPointer.x, 0,
                this._pixelUnderPointer.x + 1, 0);
            cairoContext.set_source(pattern);
            pattern.add_color_stop_rgba(0.0, 1.0, 1.0, 1.0, 1.0);
            pattern.add_color_stop_rgba(highlightEdgeThickness,
                                        0.5, 0.5, 0.5, 0.0);
            pattern.add_color_stop_rgba(1.0 - highlightEdgeThickness,
                                        0.5, 0.5, 0.5, 0.0);
            pattern.add_color_stop_rgba(1.0, 1.0, 1.0, 1.0, 1.0);
            cairoContext.rectangle(this._pixelUnderPointer.x, 0,
                                   1, this._numRows);
            cairoContext.fill();

            highlightEdgeThickness = 10.0 * this._numRows / size.height;

            pattern = new Cairo.LinearGradient(
                0, this._pixelUnderPointer.y,
                0, this._pixelUnderPointer.y + 1);
            cairoContext.set_source(pattern);
            pattern.add_color_stop_rgba(0.0, 1.0, 1.0, 1.0, 1.0);
            pattern.add_color_stop_rgba(highlightEdgeThickness,
                                        0.5, 0.5, 0.5, 0.0);
            pattern.add_color_stop_rgba(1.0 - highlightEdgeThickness,
                                        0.5, 0.5, 0.5, 0.0);
            pattern.add_color_stop_rgba(1.0, 1.0, 1.0, 1.0, 1.0);
            cairoContext.rectangle(0, this._pixelUnderPointer.y,
                                   this._numColumns, 1);
            cairoContext.fill();
        }

        cairoContext.set_source_rgb(0.0, 0.0, 0.0);

        for (i = 0; i < this._numRows + 1; ++i) {
            cairoContext.line_width = 1.0;
            if (i % 5 == 0)
                cairoContext.line_width = 3.0;
            if (i == 0 || i == this._numRows)
                cairoContext.line_width = 6.0;

            cairoContext.move_to(0, i);
            cairoContext.line_to(this._numColumns, i);
            cairoContext.save();
            cairoContext.scale(this._numColumns / size.width,
                               this._numRows / size.height);
            cairoContext.stroke();
            cairoContext.restore();
        }

        for (i = 0; i < this._numColumns + 1; ++i) {
            cairoContext.line_width = 1.0;
            if (i % 5 == 0)
                cairoContext.line_width = 3.0;
            if (i == 0 || i == this._numColumns)
                cairoContext.line_width = 6.0;

            cairoContext.move_to(i, 0);
            cairoContext.line_to(i, this._numRows);
            cairoContext.save();
            cairoContext.scale(this._numColumns / size.width,
                               this._numRows / size.height);
            cairoContext.stroke();
            cairoContext.restore();
        }

        return false;
    },

    _getPixelFromWidgetCoordinates: function(x, y) {
        var widgetSize = this.get_allocation().allocation;

        var row = Math.floor(y * this._numRows / widgetSize.height);
        var column = Math.floor(x * this._numColumns / widgetSize.width);

        if ((row < 0) || (row >= this._numRows) ||
            (column < 0) || (column >= this._numColumns)) {
            print("Unexpected row, column from click. Coding error.");
            return null;
        }

        return {y: row, x: column};
    },

    _onButtonPress: function(widget, event) {
        event = event.button;

        var pixel = this._getPixelFromWidgetCoordinates(event.x, event.y);
        if (pixel == null)
            return false;

        this._dragStartPosition = { x: pixel.x, y: pixel.y };
        this._dragEndPosition = { x: pixel.x, y: pixel.y };
        this._pixelChangesForUndo = new PixelChanges();
        this._pixelChangesForUndo.changeArray = [];

        var newColor = this._drawingColor;
        if (event.button == 1)
            newColor = this._drawingColor;
        else if (event.button == 3)
            newColor = this._BACKGROUND_COLOR;
        else if (event.button == 2)
            newColor = this._NO_COLOR;
        else
            return false;

        var prevColor = this._puzzlePixels[pixel.y][pixel.x];
        if (prevColor == newColor)
            return false;

        var pixelChanges = new PixelChanges();
        pixelChanges.changeArray = [];
        var change = { x: pixel.x,
                       y: pixel.y,
                       newColor: newColor,
                       prevColor: prevColor
                     };
        pixelChanges.changeArray.push(change);
        this._pixelChangesForUndo.changeArray.push(change);

        this._setPixelsColor(pixelChanges);

        return false;
    },

    _onButtonRelease: function(widget, event) {
        this._dragStartPosition = null;
        this._dragEndPosition = null;

        if (this._pixelChangesForUndo !== null &&
            this._pixelChangesForUndo.changeArray.length != 0) {
            this._storeUndoInformation(this._pixelChangesForUndo);
        }

        this._pixelChangesForUndo = null;

        return false;
    },

    _onMouseMove: function(widget, event) {
        event = event.motion;

        this._updatePixelUnderPointer(event);

        var newColor = this._drawingColor;
        if (event.state & Gdk.ModifierType.BUTTON1_MASK)
            newColor = this._drawingColor;
        else if (event.state & Gdk.ModifierType.BUTTON3_MASK)
            newColor = this._BACKGROUND_COLOR;
        else if (event.state & Gdk.ModifierType.BUTTON2_MASK)
            newColor = this._NO_COLOR;
        else {
            if (event.is_hint == true)
                Gdk.event_request_motions(event);
            return false;
        }

        var widgetSize = this.get_allocation().allocation;
        if (event.x < 0)
            event.x = 0;

        if (event.x >= widgetSize.width)
            event.x = widgetSize.width - 1;

        if (event.y < 0)
            event.y = 0;

        if (event.y >= widgetSize.height)
            event.y = widgetSize.height - 1;

        var pixel = this._getPixelFromWidgetCoordinates(event.x, event.y);
        if (pixel == null) {
            if (event.is_hint == true)
                Gdk.event_request_motions(event);
            return false;
        }

        var xDelta = pixel.x - this._dragStartPosition.x;
        var yDelta = pixel.y - this._dragStartPosition.y;

        if (xDelta == 0 && yDelta == 0) {
            if (event.is_hint == true)
                Gdk.event_request_motions(event);
            return false;
        }

        var extendDragBoundaries = function(isVertical) {
            var direction = "x";
            if (isVertical)
                direction = "y";

            this._dragEndPosition[direction] =
                Math.max(pixel[direction], this._dragEndPosition[direction]);
            this._dragStartPosition[direction] =
                Math.min(pixel[direction], this._dragStartPosition[direction]);
        };

        var isVertical;
        if (this._dragStartPosition.x == this._dragEndPosition.x &&
            this._dragStartPosition.y == this._dragEndPosition.y) {
            // First time, set the orientation of the drag based on
            // magnitudes of delta movement in both directions
            isVertical = Math.abs(yDelta) > Math.abs(xDelta);
        } else {
            // Once set, use the direction and only extend it in that
            // direction
            isVertical = (this._dragStartPosition.x == this._dragEndPosition.x);
        }
        extendDragBoundaries.call(this, isVertical);

        var pixelChanges = new PixelChanges();
        pixelChanges.changeArray = [];

        var increment = Math.round(yDelta / Math.abs(yDelta));

        for (var y = this._dragStartPosition.y;
             y <= this._dragEndPosition.y;
             ++y) {
            for (var x = this._dragStartPosition.x;
                 x <= this._dragEndPosition.x;
                 ++x){
                if (this._puzzlePixels[y][x] == newColor)
                    continue;

                var change = {
                    x: x,
                    y: y,
                    newColor: newColor,
                    prevColor: this._puzzlePixels[y][x]
                };
                pixelChanges.changeArray.push(change);
                this._pixelChangesForUndo.changeArray.push(change);
            }
        }

        this._setPixelsColor(pixelChanges);

        if (event.is_hint == true)
            Gdk.event_request_motions(event);

        return false;
    },

    _updatePixelUnderPointer: function(event) {
        var widgetSize = this.get_allocation().allocation;
        var pixelX = Math.floor(event.x * this._numColumns / widgetSize.width);
        var pixelY = Math.floor(event.y * this._numRows / widgetSize.height);
        if (this._pixelUnderPointer != null &&
            this._pixelUnderPointer.x == pixelX &&
            this._pixelUnderPointer.y == pixelY) {
            return;
        }

        this._pixelUnderPointer = new Pixel();
        this._pixelUnderPointer.x = pixelX;
        this._pixelUnderPointer.y = pixelY;
        this.signal.highlight_changed.emit(this._pixelUnderPointer);

        this.queue_draw();
    },

    _setPixelsColor: function(pixelChanges) {
        var previousUnknownPixelsCount = this._unknownPixelsCount;

        var emitPixelChanges = new PixelChanges();
        emitPixelChanges.puzzlePixels = this._puzzlePixels;
        emitPixelChanges.changeArray = [];

        for (var i = 0; i < pixelChanges.changeArray.length; ++i) {
            var change = pixelChanges.changeArray[i];
            if (change.prevColor == change.newColor)
                continue;

            emitPixelChanges.changeArray.push(change);

            this._puzzlePixels[change.y][change.x] = change.newColor;

            if (change.prevColor == this._NO_COLOR)
                --this._unknownPixelsCount;
            else if (change.newColor == this._NO_COLOR)
                ++this._unknownPixelsCount;
        }

        if (emitPixelChanges.changeArray.length == 0)
            return;

        this.queue_draw();

        this.signal.puzzle_changed.emit(emitPixelChanges);

        if (this._unknownPixelsCount == 0)
            this.signal.puzzle_full_status_changed.emit(true);

        if (previousUnknownPixelsCount == 0 && this._unknownPixelsCount != 0)
            this.signal.puzzle_full_status_changed.emit(false);
    },

    revealRemaining: function() {
        for (var y = 0; y < this._numRows; ++y) {
            for (var x = 0; x < this._numColumns; ++x) {
                if (this._puzzlePixels[y][x] == this._NO_COLOR)
                    this._puzzlePixels[y][x] = this._BACKGROUND_COLOR;
            }
        }

        this.queue_draw();
    },

    _onStateFlagsChanged: function(widget, previousState) {
        var previousIsSensitive = !(previousState &
                                    Gtk.StateFlags.INSENSITIVE);
        var currentIsSensitive = this.sensitive;
        if (previousIsSensitive == true && currentIsSensitive == false) {
            this._pixelUnderPointer = null;
            this.signal.highlight_changed.emit(this._pixelUnderPointer);
            this.queue_draw();
        }
    },

    _storeUndoInformation: function(pixelChanges) {
        var previousUndoStackLength = this._undoStack.length;
        var previousRedoStackLength = this._redoStack.length;

        this._redoStack = [];
        this._undoStack.push(pixelChanges);

        this._emitUndoRedoAvailable(previousUndoStackLength,
                                    previousRedoStackLength);
    },

    undo: function() {
        if (this.sensitive == false)
            return;

        if (this._undoStack.length == 0)
            return;

        var previousUndoStackLength = this._undoStack.length;
        var previousRedoStackLength = this._redoStack.length;

        var pixelChanges = this._undoStack.pop();
        this._redoStack.push(pixelChanges);

        var undoPixelChanges = new PixelChanges();
        undoPixelChanges.changeArray = [];
        pixelChanges.changeArray.forEach(
            function(change) {
                var newChange = {};
                for (var key in change) {
                    newChange[key] = change[key];
                }

                newChange.prevColor = change.newColor;
                newChange.newColor = change.prevColor;
                undoPixelChanges.changeArray.push(newChange);
            }
        );

        this._setPixelsColor(undoPixelChanges);

        this._emitUndoRedoAvailable(previousUndoStackLength,
                                    previousRedoStackLength);
    },

    redo: function() {
        if (this.sensitive == false)
            return;

        if (this._redoStack.length == 0)
            return;

        var previousUndoStackLength = this._undoStack.length;
        var previousRedoStackLength = this._redoStack.length;

        var pixelChanges = this._redoStack.pop();
        this._undoStack.push(pixelChanges);
        this._setPixelsColor(pixelChanges);

        this._emitUndoRedoAvailable(previousUndoStackLength,
                                    previousRedoStackLength);
    },

    _emitUndoRedoAvailable: function(previousUndoStackLength,
                                     previousRedoStackLength) {
        if ((this._undoStack.length == 0 && previousUndoStackLength != 0) ||
            (this._undoStack.length != 0 && previousUndoStackLength == 0) ||
            (this._redoStack.length == 0 && previousRedoStackLength != 0) ||
            (this._redoStack.length != 0 && previousRedoStackLength == 0)) {
            var undoAvailable = (this._undoStack.length != 0);
            var redoAvailable = (this._redoStack.length != 0);
            this.signal.undo_redo_available.emit(undoAvailable,
                                                 redoAvailable);
        }
    }
});

var _PixelChanges;
var PixelChanges = new GType(_PixelChanges = {
    parent: GObject.TYPE_OBJECT,
    name: "PixelChanges"
});

var _Pixel;
var Pixel = new GType(_Pixel = {
    parent: GObject.TYPE_OBJECT,
    name: "Pixel"
});
