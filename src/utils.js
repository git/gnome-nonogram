/*
* Copyright (C) 2011 Sunil Mohan Adapa <sunil@medhas.org>.
* Copyright (C) 2011 O S K Chaitanya <osk@medhas.org>.
*
* Author: Sunil Mohan Adapa <sunil@medhas.org>
*         O S K Chaitanya <osk@medhas.org>
*
* This file is part of GNOME Nonogram.
*
* GNOME Nonogram is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* GNOME Nonogram is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with GNOME Nonogram. If not, see <http://www.gnu.org/licenses/>.
*/

const GLib = imports.gi.GLib;

const Config = imports.config;

function classInitBoilerplate(klass, prototype, classDefinitionHash) {
    for (key in classDefinitionHash) {
        if (key == "init" ||
            key == "class_init" ||
            key == "signals" ||
            key == "properties") {
            continue;
        }

        prototype[key] = classDefinitionHash[key];
    }
}

function getPuzzleSetAndPuzzleNames(puzzleFile) {
    var puzzleName = GLib.path_get_basename(puzzleFile);
    var puzzleDirname = GLib.path_get_dirname(puzzleFile);
    var puzzleSetName = GLib.path_get_basename(puzzleDirname);

    return { puzzleName: puzzleName, puzzleSetName: puzzleSetName };
}

function getSaveFilePathForPuzzle(puzzleFile) {
    var names = getPuzzleSetAndPuzzleNames(puzzleFile);
    var saveFilePath = GLib.build_filenamev([Config.saveDataDir,
                                             names.puzzleSetName,
                                             names.puzzleName]);
    return saveFilePath;
}
