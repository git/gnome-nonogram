/*
* Copyright (C) 2011 Sunil Mohan Adapa <sunil@medhas.org>.
* Copyright (C) 2011 O S K Chaitanya <osk@medhas.org>.
*
* Author: Sunil Mohan Adapa <sunil@medhas.org>
*         O S K Chaitanya <osk@medhas.org>
*
* This file is part of GNOME Nonogram.
*
* GNOME Nonogram is free software: you can redistribute it and/or modify
* it under the terms of the GNU General Public License as published by
* the Free Software Foundation, either version 3 of the License, or
* (at your option) any later version.
*
* GNOME Nonogram is distributed in the hope that it will be useful,
* but WITHOUT ANY WARRANTY; without even the implied warranty of
* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
* GNU General Public License for more details.
*
* You should have received a copy of the GNU General Public License
* along with GNOME Nonogram. If not, see <http://www.gnu.org/licenses/>.
*/

const Lang = imports.lang;
const Cairo = imports.cairo;
const GObject = imports.gi.GObject;
const Gdk = imports.gi.Gdk;
const Gtk = imports.gi.Gtk;

const Utils = imports.utils;

var _ClueTable;
var ClueTable = new GType(_ClueTable = {
    parent: Gtk.Table.type,
    name: "ClueTable",

    signals: [{name: "all_clues_satisfied"}],

    _BACKGROUND_COLOR: 0xffffffff,
    _NO_COLOR: 0,

    _solutionPuzzlePixels: null,
    _numPixelRows: null,
    _numPixelColumns: null,

    _clues: null,
    _numClueLines: null,
    _maxClueLineLength: null,
    _clueLineIsSatisfied: [],
    allClueLinesAreSatisfied: false,
    _highlightedPixel: null,

    class_init: function(klass, prototype) {
        Utils.classInitBoilerplate(klass, prototype, _ClueTable);
    },

    init: function() {
        this.signal.draw.connect(Lang.bind(this, this._onDraw));
    },

    initializeClues: function(solutionPuzzlePixels, currentPuzzlePixels) {
        this._solutionPuzzlePixels = solutionPuzzlePixels;
        this._numPixelRows = this._solutionPuzzlePixels.length;
        this._numPixelColumns = this._solutionPuzzlePixels[0].length;

        this._generateClues();

        // Remove existing children
        this.foreach(
            function(child) {
                child.get_parent().remove(child);
            }
        );

        // Find new dimensions
        this._numClueLines = this._clues.length;
        this._maxClueLineLength = 0;
        for (var i = 0; i < this._numClueLines; ++i) {
            if (this._maxClueLineLength < this._clues[i].length) {
                this._maxClueLineLength = this._clues[i].length;
            }
        }

        // Resize
        this._resize();

        // Create new children
        this._populateClues();

        // Mark satisfied clues
        this._initializeSatisfiedClues(currentPuzzlePixels);

        // Show the new children
        this.show_all();
    },

    _initializeSatisfiedClues: function(puzzlePixels) {
        if (puzzlePixels == null) {
            this.reset();
        } else {
            this.allClueLinesAreSatisfied = false;
            this._clueLineIsSatisfied = [];
            var changedLines = {};
            for (var i = 0; i < this._numClueLines; ++i) {
                var clueLineSatisfied = false;
                if (this._clues[i].length == 0) {
                    clueLineSatisfied = true;
                }
                this._clueLineIsSatisfied.push(clueLineSatisfied);

                changedLines[i] = true;
            }

            this._onPuzzleLinesChanged(puzzlePixels, changedLines);
        }
    },

    reset: function() {
        this.allClueLinesAreSatisfied = false;
        this._clueLineIsSatisfied = [];
        for (var i = 0; i < this._numClueLines; ++i) {
            // Rows and columns which are empty should be marked as
            // satisfied by default
            var clueLineSatisfied = false;
            if (this._clues[i].length == 0) {
                clueLineSatisfied = true;
            }
            this._clueLineIsSatisfied.push(clueLineSatisfied);

            for (var j = 0; j < this._clues[i].length; ++j) {
                this._setClueSatisfied(i, j, false);
            }
        }
    },

    _resize: function() {
        // Virtual function: override in derived class
    },

    _addLabel: function(label, clueLineIndex, clueIndex) {
        // Virtual function: override in derived class
    },

    _generateClues: function() {
        // Virtual function: override in derived class
        this._clues = null;
    },

    _generateSingleLineCluesOriented: function () {
        // Virtual function: override in derived class
    },

    _generateSingleLineClues: function(puzzlePixels,
                                       foregroundOnly,
                                       rowStart,
                                       columnStart,
                                       rowIncrement,
                                       columnIncrement) {
        var singleLineClues = [];
        var currentColor = this._BACKGROUND_COLOR;
        var currentCount = 0;

        for (var x = columnStart, y = rowStart;
             true;
             x += columnIncrement, y += rowIncrement) {
            var endReached = ((x >= this._numPixelColumns) ||
                              (y >= this._numPixelRows));

            if (endReached || puzzlePixels[y][x] != currentColor) {
                if (foregroundOnly == false ||
                    (currentColor != this._BACKGROUND_COLOR &&
                     currentColor != this._NO_COLOR)) {
                    singleLineClues.push({color: currentColor,
                                          count: currentCount,
                                          isSatisfied: false});
                }

                if (endReached) {
                    break;
                }

                currentColor = puzzlePixels[y][x];
                currentCount = 0;
            }

            ++currentCount;
        }

        return singleLineClues;
    },

    _markSatisfiedCluesInLine: function(line, lineClues) {
        var solutionClues = this._clues[line];

        // Reset satisfied flags
        for (var i = 0; i < solutionClues.length; ++i) {
            this._setClueSatisfied(line, i, false);
        }

        // Move forward from beginning and mark satisfied flags
        for (var solutionClueIndex = 0;
             lineClues.length > 0 && solutionClueIndex < solutionClues.length;
            ) {
            var firstClue = lineClues[0];
            if (firstClue.color == this._BACKGROUND_COLOR) {
                lineClues.shift();
                continue;
            }

            if (firstClue.color == this._NO_COLOR) {
                break;
            }

            if (firstClue.color != solutionClues[solutionClueIndex].color ||
                firstClue.count != solutionClues[solutionClueIndex].count) {
                break;
            }

            this._setClueSatisfied(line, solutionClueIndex, true);
            ++solutionClueIndex;
            lineClues.shift();
        }

        // Move back from end and mark satisfied flags
        for (solutionClueIndex = solutionClues.length - 1;
             lineClues.length > 0 && solutionClueIndex >= 0;
            ) {
            if (solutionClues[solutionClueIndex].isSatisfied) {
                break;
            }

            var lastClue = lineClues[lineClues.length - 1];
            if (lastClue.color == this._BACKGROUND_COLOR) {
                lineClues.pop();
                continue;
            }

            if (lastClue.color == this._NO_COLOR) {
                break;
            }

            if (lastClue.color != solutionClues[solutionClueIndex].color ||
                lastClue.count != solutionClues[solutionClueIndex].count) {
                break;
            }

            this._setClueSatisfied(line, solutionClueIndex, true);
            --solutionClueIndex;
            lineClues.pop();
        }

        // Handle excess strips marked by the player:
        // Even if all clues are marked satisfied, if the line has already
        // been marked as not satisfied, it means the player has marked excess
        // strips.
        // In that case mark the entire line as unsatisfied.
        var allCluesMarkedSatisfied = true;
        for (i = 0; i < solutionClues.length; ++i) {
            if (solutionClues[i].isSatisfied == false) {
                allCluesMarkedSatisfied = false;
                break;
            }
        }

        if (allCluesMarkedSatisfied &&
            this._clueLineIsSatisfied[line] == false) {
            for (i = 0; i < solutionClues.length; ++i) {
                this._setClueSatisfied(line, i, false);
            }
        }
    },

    _populateClues: function() {
        for (var clueLineIndex = 0;
             clueLineIndex < this._numClueLines;
             ++clueLineIndex) {
            var array = this._clues[clueLineIndex];

            for (var clueIndex = 0;
                 clueIndex < this._maxClueLineLength;
                 ++clueIndex) {
                var labelText = " ";
                var arrayIndex =
                    clueIndex - this._maxClueLineLength + array.length;
                if (arrayIndex >= 0) {
                    labelText = array[arrayIndex].count.toString();

                    var labelColor = array[arrayIndex].color.toString(16);
                    labelColor = "00000000".slice(0, -labelColor.length) +
                                 labelColor;
                    labelColor = labelColor.slice(0, -2);

                    labelText = "<span color='#" +
                                labelColor +
                                "'>" +
                                labelText +
                                "</span>";
                }

                var label = new Gtk.Label({label: labelText,
                                           xalign: this.labelTextXAlign,
                                           use_markup: true});

                if (arrayIndex >= 0) {
                    array[arrayIndex].label = label;
                    array[arrayIndex].bareLabelText = labelText;
                }

                this._addLabel(label, clueLineIndex, clueIndex);
            }
        }
    },

    onPuzzleChanged: function(pixelGrid, pixelChanges) {
        // Virtual function: override in derived class
    },

    onHighlightChanged: function(pixelGrid, pixel) {
        this._highlightedPixel = pixel;
        this.queue_draw();
    },

    _onPuzzleLinesChanged: function(puzzlePixels, changedLines) {
        for (var line in changedLines) {
            // Keys of the hash are strings, we need integers
            line = parseInt(line);
            var singleLineClues =
                this._generateSingleLineCluesOriented(puzzlePixels, true, line);

            this._clueLineIsSatisfied[line] = true;
            if (singleLineClues.length != this._clues[line].length) {
                this._clueLineIsSatisfied[line] = false;
                continue;
            }

            for (var i = 0; i < this._clues[line].length; ++i) {
                if (this._clues[line][i].color != singleLineClues[i].color ||
                    this._clues[line][i].count != singleLineClues[i].count) {
                    this._clueLineIsSatisfied[line] = false;
                    break;
                }
            }
        }

        for (line in changedLines) {
            line = parseInt(line);
            var lineClues = this._generateSingleLineCluesOriented(puzzlePixels,
                                                                  false,
                                                                  line);
            this._markSatisfiedCluesInLine(line, lineClues);
        }
    },

    _setClueSatisfied: function(clueLineIndex, clueIndex, isSatisfied) {
        this._clues[clueLineIndex][clueIndex].isSatisfied = isSatisfied;

        var label = this._clues[clueLineIndex][clueIndex].label;
        var labelText = this._clues[clueLineIndex][clueIndex].bareLabelText;

        if (isSatisfied == false) {
            labelText = "<b><big>" + labelText + "</big></b>";
        } else {
            labelText = "<s><small>" + labelText + "</small></s>";
        }

        label.set_label(labelText);
    },

    markAllCluesSatisfied: function() {
        for (var i = 0; i < this._numClueLines; ++i) {
            for (var j = 0; j < this._clues[i].length; ++j) {
                this._setClueSatisfied(i, j, true);
            }
        }
    },

    _checkFinish: function() {
        this.allClueLinesAreSatisfied = true;
        for (var i = 0; i < this._clueLineIsSatisfied.length; ++i) {
            if (this._clueLineIsSatisfied[i] == false) {
                this.allClueLinesAreSatisfied = false;
                break;
            }
        }

        if (this.allClueLinesAreSatisfied) {
            this.signal.all_clues_satisfied.emit();
        }
    },

    _onDraw: function() {
        // Virtual function: override in derived class
    }
});

var _RowClueTable;
var RowClueTable = new GType(_RowClueTable = {
    parent: ClueTable.type,
    name: "RowClueTable",

    class_init: function(klass, prototype) {
        prototype.labelTextXAlign = 1.0;

        Utils.classInitBoilerplate(klass, prototype, _RowClueTable);
    },

    _resize: function() {
        this.resize(this._numClueLines, this._maxClueLineLength);
    },

    _addLabel: function(label, clueLineIndex, clueIndex) {
        var x = clueIndex;
        var y = clueLineIndex;

        this.attach(label,
                    x,
                    x + 1,
                    y,
                    y + 1,
                    Gtk.AttachOptions.EXPAND | Gtk.AttachOptions.FILL,
                    Gtk.AttachOptions.EXPAND | Gtk.AttachOptions.FILL,
                    5,
                    2);
    },

    _generateClues: function() {
        this._clues = [];
        for (var y = 0; y < this._numPixelRows; ++y) {
            var singleRowClues =
                this._generateSingleLineCluesOriented(
                    this._solutionPuzzlePixels, true, y);

            this._clues.push(singleRowClues);
        }
    },

    _generateSingleLineCluesOriented: function(puzzlePixels,
                                               foregroundOnly,
                                               line) {
        return this._generateSingleLineClues(puzzlePixels,
                                             foregroundOnly,
                                             line,
                                             0,
                                             0,
                                             1);
    },

    onPuzzleChanged: function(pixelGrid, pixelChanges) {
        var changeArray = pixelChanges.changeArray;

        var changedLines = {};
        changeArray.forEach(
            function (change) {
                changedLines[change.y] = true;
            }
        );

        this._onPuzzleLinesChanged(pixelChanges.puzzlePixels, changedLines);

        this._checkFinish();
    },

    _onDraw: function(widget, cairoContextStruct) {
        if (this._highlightedPixel == null) {
            return false;
        }

        var cairoContext = new Cairo.Context.steal(cairoContextStruct);
        this._cairoContext = cairoContext; // FIXME: this is only to keep a ref
                                           // until the next call

        var height = this.get_allocated_height();
        var width = this.get_allocated_width();

        cairoContext.save();
        cairoContext.scale(width, height / this._numPixelRows);

        var highlightEdgeThickness = 10.0 * this._numPixelRows / height;

        var pattern = new Cairo.LinearGradient(0,
                                               this._highlightedPixel.y,
                                               0,
                                               this._highlightedPixel.y + 1);
        cairoContext.set_source(pattern);
        pattern.add_color_stop_rgba(0.0, 0.5, 0.5, 0.5, 1.0);
        pattern.add_color_stop_rgba(highlightEdgeThickness, 1.0, 1.0, 1.0, 0.0);
        pattern.add_color_stop_rgba(1 - highlightEdgeThickness,
                                    1.0,
                                    1.0,
                                    1.0,
                                    0.0);
        pattern.add_color_stop_rgba(1.0, 0.5, 0.5, 0.5, 1.0);
        cairoContext.rectangle(0, this._highlightedPixel.y, 1, 1);
        cairoContext.fill();
        cairoContext.restore();

        return false;
    }
});

var _ColumnClueTable;
var ColumnClueTable = new GType(_ColumnClueTable = {
    parent: ClueTable.type,
    name: "ColumnClueTable",

    class_init: function(klass, prototype) {
        prototype.labelTextXAlign = 0.5;

        Utils.classInitBoilerplate(klass, prototype, _ColumnClueTable);
    },

    _resize: function() {
        this.resize(this._maxClueLineLength, this._numClueLines);
    },

    _addLabel: function(label, clueLineIndex, clueIndex) {
        var x = clueLineIndex;
        var y = clueIndex;

        this.attach(label,
                    x,
                    x + 1,
                    y,
                    y + 1,
                    Gtk.AttachOptions.EXPAND | Gtk.AttachOptions.FILL,
                    Gtk.AttachOptions.EXPAND | Gtk.AttachOptions.FILL,
                    5,
                    2);
    },

    _generateClues: function() {
        this._clues = [];
        for (var x = 0; x < this._numPixelColumns; ++x) {
            var singleColumnClues =
                this._generateSingleLineCluesOriented(
                    this._solutionPuzzlePixels, true, x);

            this._clues.push(singleColumnClues);
        }
    },

    _generateSingleLineCluesOriented: function(puzzlePixels,
                                               foregroundOnly,
                                               line) {
        return this._generateSingleLineClues(puzzlePixels,
                                             foregroundOnly,
                                             0,
                                             line,
                                             1,
                                             0);
    },

    onPuzzleChanged: function(pixelGrid, pixelChanges) {
        var changeArray = pixelChanges.changeArray;

        var changedLines = {};
        changeArray.forEach(
            function (change) {
                changedLines[change.x] = true;
            }
        );

        this._onPuzzleLinesChanged(pixelChanges.puzzlePixels, changedLines);

        this._checkFinish();
    },

    _onDraw: function(widget, cairoContextStruct) {
        if (this._highlightedPixel == null) {
            return false;
        }

        var cairoContext = new Cairo.Context.steal(cairoContextStruct);
        this._cairoContext = cairoContext; // FIXME: this is only to keep a ref
                                           // until the next call

        var height = this.get_allocated_height();
        var width = this.get_allocated_width();

        cairoContext.save();
        cairoContext.scale(width / this._numPixelColumns, height);

        var highlightEdgeThickness = 10.0 * this._numPixelColumns / width;

        var pattern = new Cairo.LinearGradient(this._highlightedPixel.x,
                                               0,
                                               this._highlightedPixel.x + 1,
                                               0);
        cairoContext.set_source(pattern);
        pattern.add_color_stop_rgba(0.0, 0.5, 0.5, 0.5, 1.0);
        pattern.add_color_stop_rgba(highlightEdgeThickness, 1.0, 1.0, 1.0, 0.0);
        pattern.add_color_stop_rgba(1 - highlightEdgeThickness,
                                    1.0,
                                    1.0,
                                    1.0,
                                    0.0);
        pattern.add_color_stop_rgba(1.0, 0.5, 0.5, 0.5, 1.0);
        cairoContext.rectangle(this._highlightedPixel.x, 0, 1, 1);
        cairoContext.fill();
        cairoContext.restore();

        return false;
    }
});
