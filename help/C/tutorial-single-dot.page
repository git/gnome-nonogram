<!--
Copyright (C) 2011 Sunil Mohan Adapa <sunil@medhas.org>.
Copyright (C) 2011 O S K Chaitanya <osk@medhas.org>.

Author: Sunil Mohan Adapa <sunil@medhas.org>
         O S K Chaitanya <osk@medhas.org>

This file is part of GNOME Nonogram.

GNOME Nonogram is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GNOME Nonogram is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GNOME Nonogram. If not, see <http://www.gnu.org/licenses/>.
-->

<page xmlns="http://projectmallard.org/1.0/"
      type="topic"
      id="tutorial-single-dot">
  <info>
    <title type="sort">50</title>
    <link type="guide" xref="tutorial"/>
    <link type="next" xref="tutorial-partial-line-filling"/>
    <credit type="author copyright">
      <name>Sunil Mohan Adapa</name>
      <email>sunil@medhas.org</email>
      <years>2011</years>
    </credit>
    <credit type="author copyright">
      <name>O S K Chaitanya</name>
      <email>osk@medhas.org</email>
      <years>2011</years>
    </credit>
    <license>
      <p>GNU General Public License 3.0 or any later version</p>
    </license>
  </info>
  <title>Using row and column clues together</title>
  <list>
    <item>
      <p>
        Some squares can be filled by seeing the row and column clues
        together.
      </p>
    </item>
  </list>
  <p>
    Applying the above logic to the <em>Single Dot</em> tutorial puzzle:
  </p>
  <media type="image" src="figures/tutorial5-1.png">
    <p>An image of the <em>Single Dot</em> tutorial puzzle at the beginning</p>
  </media>
  <p>
    the puzzle can be solved like this:
  </p>
  <media type="image" src="figures/tutorial5-2.png">
    <p>An image of the <em>Single Dot</em> tutorial puzzle after applying the
    above logic to solve it</p>
  </media>
</page>
