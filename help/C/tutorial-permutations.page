<!--
Copyright (C) 2011 Sunil Mohan Adapa <sunil@medhas.org>.
Copyright (C) 2011 O S K Chaitanya <osk@medhas.org>.

Author: Sunil Mohan Adapa <sunil@medhas.org>
         O S K Chaitanya <osk@medhas.org>

This file is part of GNOME Nonogram.

GNOME Nonogram is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

GNOME Nonogram is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with GNOME Nonogram. If not, see <http://www.gnu.org/licenses/>.
-->

<page xmlns="http://projectmallard.org/1.0/"
      type="topic"
      id="tutorial-permutations">
  <info>
    <title type="sort">90</title>
    <link type="guide" xref="tutorial"/>
    <link type="next" xref="tutorial-multiple-solutions"/>
    <credit type="author copyright">
      <name>Sunil Mohan Adapa</name>
      <email>sunil@medhas.org</email>
      <years>2011</years>
    </credit>
    <credit type="author copyright">
      <name>O S K Chaitanya</name>
      <email>osk@medhas.org</email>
      <years>2011</years>
    </credit>
    <license>
      <p>GNU General Public License 3.0 or any later version</p>
    </license>
  </info>
  <title>Eliminating squares which lead to an unsolvable state</title>
  <list>
    <item>
      <p>
        It can be determined that some squares are background squares. This can
        be done if filling them with foreground color and trying to fill the
        rest of the puzzle leads to a situation where it is impossible to
        satisfy all the number clues.
      </p>
    </item>
  </list>
  <p>
    Applying logic from earlier puzzles to <em>Permutations</em> tutorial
    puzzle:
  </p>
  <media type="image" src="figures/tutorial9-1.png">
    <p>An image of the <em>Permutations</em> tutorial puzzle at the
    beginning</p>
  </media>
  <p>
  the puzzle gets partially filled like this:
  </p>
  <media type="image" src="figures/tutorial9-2.png">
    <p>An image of the <em>Permutations</em> tutorial puzzle after applying
    logic from earlier puzzles</p>
  </media>
  <p>
    Filling the top left square of the puzzle leads to an impossible to solve
    configuration like this:
  </p>
  <media type="image" src="figures/tutorial9-3.png">
    <p>An image of the <em>Permutations</em> tutorial puzzle after filling the
    top left square and the subsequent unsolvable state of the puzzle</p>
  </media>
  <p>
    Only one possibility remains for the top left square:
  </p>
  <media type="image" src="figures/tutorial9-4.png">
    <p>An image of the <em>Permutations</em> tutorial puzzle after filling the
    top left square with background color</p>
  </media>
</page>
