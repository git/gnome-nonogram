#!/bin/sh

which gnome-autogen.sh || {
    echo "You need to install the gnome-common package."
    exit 1
}

gnome-autogen.sh
