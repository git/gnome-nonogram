#
# Copyright (C) 2011 Sunil Mohan Adapa <sunil@medhas.org>.
# Copyright (C) 2011 O S K Chaitanya <osk@medhas.org>.
# Copyright (C) 2011 GNOME Games developers
#
# Author: Sunil Mohan Adapa <sunil@medhas.org>
#         O S K Chaitanya <osk@medhas.org>
#
# This file is part of GNOME Nonogram.
#
# GNOME Nonogram is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# GNOME Nonogram is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with GNOME Nonogram. If not, see <http://www.gnu.org/licenses/>.
#

# Icon installation code is taken from gnome-games package

NULL =

pkgdatadir = $(datadir)/games/$(PACKAGE)
defaulticonsdir = $(pkgdatadir)/icons

noinst_DATA = \
	README \
	$(NULL)

dist_defaulticons_DATA = \
	unsolved-puzzle.png \
	$(NULL)

dist_noinst_DATA = \
	hicolor_apps_16x16_gnome-nonogram.png \
	hicolor_apps_24x24_gnome-nonogram.png \
	hicolor_apps_32x32_gnome-nonogram.png \
	hicolor_apps_48x48_gnome-nonogram.png \
	hicolor_apps_64x64_gnome-nonogram.png \
	hicolor_apps_128x128_gnome-nonogram.png \
	hicolor_apps_scalable_gnome-nonogram.svg \
	$(NULL)

install-icons:
	for icon in $(dist_noinst_DATA); do \
		THEME=`echo $$icon | cut -d_ -f1`; \
		CONTEXT=`echo $$icon | cut -d_ -f2`; \
		SIZE=`echo $$icon | cut -d_ -f3`; \
		ICONFILE=`echo $$icon | cut -d_ -f4`; \
		mkdir -p $(DESTDIR)$(datadir)/icons/$$THEME/$$SIZE/$$CONTEXT; \
		$(INSTALL_DATA) $(srcdir)/$$icon $(DESTDIR)$(datadir)/icons/$$THEME/$$SIZE/$$CONTEXT/$$ICONFILE; \
	done

uninstall-icons:
	-for icon in $(dist_noinst_DATA); do \
		THEME=`echo $$icon | cut -d_ -f1`; \
		CONTEXT=`echo $$icon | cut -d_ -f2`; \
		SIZE=`echo $$icon | cut -d_ -f3`; \
		ICONFILE=`echo $$icon | cut -d_ -f4`; \
		rm -f $(DESTDIR)$(datadir)/icons/$$THEME/$$SIZE/$$CONTEXT/$$ICONFILE; \
	done

install-data-local: install-icons
uninstall-local: uninstall-icons

install-data-hook: update-icon-cache
uninstall-hook: update-icon-cache

gtk_update_icon_cache = gtk-update-icon-cache -f -t
theme = hicolor

update-icon-cache:
	@-if test -z "$(DESTDIR)"; then \
		echo "Updating Gtk icon cache."; \
		$(gtk_update_icon_cache) $(datadir)/icons/$(theme); \
	else \
		echo "*** Icon cache not updated.  After (un)install, run this:"; \
		echo "***   $(gtk_update_icon_cache) $(datadir)/icons/$(theme)"; \
	fi

.PHONY: install-icons uninstall-icons update-icon-cache